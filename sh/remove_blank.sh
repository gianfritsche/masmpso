## remove_blank.sh
# remove blank lines from FUN files

#!/bin/bash 

cd ../SMPSODDStudy

files=`find -name FUN*`

for i in $files
do
  sed '/^$/d' $i > $i.out
  mv  $i.out $i
  echo $i
done
