#!/bin/bash

export PATH=/usr/local/cuda-6.0/bin:$PATH
export LD_LIBRARY_PATH=/usr/local/cuda-6.0/lib64:/usr/local/nvidia-latest/lib64:$LD_LIBRARY_PATH


nomeSaida="lowlevel"

mkdir -p "results/$nomeSaida"
for problem in 1 2 3 4 5 6 7; do
        for objectives in 2 3 5 10 15 20; do
                for a in cd ideal mga; do
                        for l in cd nwsum sigma; do
                                paramFile="$nomeSaida-cd-$a-DTLZ$problem-$objectives.param"
                                file="results/$nomeSaida/cd-$a/DTLZ$problem-$objectives"
                                mkdir -p $file
                                echo $file

                                echo "outName=$file" > $paramFile
                                echo "problem=dtlz$problem" >> $paramFile
                                echo "objectiveNumber=$objectives" >>$paramFile
                                echo "population=100" >> $paramFile
                                echo "repository=100" >> $paramFile
                                echo "swarms=1" >> $paramFile
                                echo "iterations=100" >> $paramFile
                                echo "runs=30" >> $paramFile
                                echo "IMulti=false" >> $paramFile
                                echo "novo=false" >> $paramFile
                                echo "leader=$l" >> $paramFile
                                echo "archiver=$a" >> $paramFile
                                echo "algorithm=smpso" >> $paramFile

                                ./cuda_mopso.out $paramFile
                        done
                done
        done
done

