## titles.sh
# rename smpso configurations

#!/bin/bash 

cd ../SMPSODDStudy

for m in 2 3 5 10 15 20; do # for each number of objectives
	echo $m
	cd $m"/data/"
	mv "novo(0,0)" H-MOPSO
	pwd
	mv "smpso(2,2)" CD-CD
	mv "smpso(2,3)" CD-Ideal
	mv "smpso(2,4)" CD-MGA
	mv "smpso(3,2)" NWSum-CD
	mv "smpso(3,3)" NWSum-Ideal
	mv "smpso(3,4)" NWSum-MGA
	mv "smpso(4,2)" Sigma-CD
	mv "smpso(4,3)" Sigma-Ideal
	mv "smpso(4,4)" Sigma-MGA
	cd ../..
done

