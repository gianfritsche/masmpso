#!/bin/bash

# backup.sh

function log () {
	echo -e "\e[1;34m $1 \e[0m"
}

log "creating tunnel..."
ssh -fN -l gmfritsche -L 2222:hydra:22 ssh.c3sl.ufpr.br &

log "backup from orval ~/cuda/ to samsung ~/Documents/cuda/"
rsync -auzh --info=progress2,stats1 gmfritsche@ssh.c3sl.ufpr.br:cuda/ ~/Documents/cuda/

# edit ( ~/.ssh/config ) to looks like:
# Host hydra
# HostName localhost
# Port 2222
# HostKeyAlias hydra
# User gian

log "backup from hydra ~/masmpso to samsung ~/Documents/hydra/"
rsync -auzh --info=progress2,stats1 hydra:~/masmpso/ ~/Documents/hydra/

log "backup from samsung ~/masmpso to hydra ~/samsung/"
rsync -auzh --info=progress2,stats1 ~/masmpso/ hydra:~/samsung/

