#!/bin/bash

nomeSaida="novo"

mkdir -p "results/$nomeSaida"
for problem in 1 2 3 4 5 6 7; do
        for objectives in 2 3 5 10 15 20; do
                paramFile="$nomeSaida-DTLZ$problem-$objectives.param"
                file="results/$nomeSaida/DTLZ$problem-$objectives"
                mkdir -p $file
                echo $file

                echo "outName=$file" > $paramFile
                echo "problem=dtlz$problem" >> $paramFile
                echo "objectiveNumber=$objectives" >>$paramFile
                echo "population=100" >> $paramFile
                echo "repository=100" >> $paramFile
                echo "swarms=1" >> $paramFile
                echo "iterations=100" >> $paramFile
                echo "runs=30" >> $paramFile
                echo "IMulti=false" >> $paramFile
                echo "novo=true" >> $paramFile
                echo "leader=0" >> $paramFile
                echo "archiver=0" >> $paramFile
                echo "algorithm=hmopso" >> $paramFile

                ./cuda_mopso.out $paramFile

        done
done

