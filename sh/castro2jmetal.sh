## castro2jmetal.sh
# convert Castro output structure to jmetal structure

#!/bin/bash       

# cd ../

# for a in 2 3 4; do # for each archiver
# 	for s in 2 3 4; do # for each leader selection
# 		for p in `seq 1 7`; do # for each problem
# 			for m in 2 3 5 10 15 20; do # for each number of objectives
# 			    input="CastroStudy/novo-smpso/smpso/smpso("$a","$s")DTLZ"$p"-"$m"_fronts.txt"
# 				output="SMPSODDStudy/"$m"/data/smpso("$a","$s")/DTLZ"$p"/"
# 				echo $input
# 				echo $output
# 				mkdir -p $output
# 				csplit -s $input '/^$/' '{*}' -f $output"FUN." -n 1
# 			done
# 		done
#     done  
# done 

for p in `seq 1 7`; do # for each problem
	for m in 2 3 5 10 15 20; do # for each number of objectives
	    input="DTLZ"$p"-"$m"_fronts.txt"
		output="SMPSODDStudy/"$m"/data/CUDA/DTLZ"$p"/"
		echo $input
		echo $output
		mkdir -p $output
		csplit -s $input '/^$/' '{*}' -f $output"FUN." -n 1
    done  
done 

# csplit -s teste.txt '/^$/' '{*}' -f FUN. -n 1
# mkdir -p /a/b/c/d/etc/
# SMPSODDStudy/10/data/SMPSODD/DTLZ2/

for p in `seq 1 7`; do # for each problem
	for m in 2 3 5 10 15 20; do # for each number of objectives
		path="SMPSODDStudy/"$m"/data/CUDA/DTLZ"$p"/"
		echo $path
		cd $path 
		for ((f=0; f < 30 ; f++)); do	
			mv FUN.$((f*2)) FUN.$f
		done
		cd -
	done
done
