package org.uma.jmetal.qualityindicator.calculator;

import org.uma.jmetal.core.SolutionSet;
import org.uma.jmetal.qualityindicator.Hypervolume;
import org.uma.jmetal.qualityindicator.R2;

/**
 * This class was designed to help the calculation of Hypervolume.
 * The idea is to add all available Pareto fronts into an <b>internal
 * population</b>, in order to perform an exact normalization.
 * This internal population is used for assessing the max and min
 * values for the normalization. It is useful when you don't know
 * what are these values and must use the max known and min known
 * values.
 * <br/>
 * <br/>
 * If the intention is to compare the fronts A, B and C, then the
 * addParetoFront() method must be invoked three times giving A, B
 * and then C as input.
 */
public class CalculatorImpl extends Calculator{

  private final Hypervolume hypervolume;
  private final R2 r2;


  private final int numberOfObjectives;
  private final double offset;
  
   /**
   * Constructor for the hypervolume calculator.
   * 
   * @param numberOfObjectives the number of objectives for the problem.
   */
  public CalculatorImpl(int numberOfObjectives) {
    this(numberOfObjectives, 0.01);
  }

  public CalculatorImpl(int numberOfObjectives, String file ) {
    this(numberOfObjectives, 0.01, file);
  }
  
  /**
   * Constructor for the hypervolume calculator.
   * 
   * @param numberOfObjectives the number of objectives for the problem.
   * @param offset offset for the hypervolume reference point.
   */
  public CalculatorImpl(int numberOfObjectives, double offset) {
    this(numberOfObjectives, offset, "");
  }

  public CalculatorImpl(int numberOfObjectives, double offset, String file) {
    this.numberOfObjectives = numberOfObjectives;
    this.hypervolume = new Hypervolume();
    this.offset = offset;
    if (!file.equals("")) {
      this.r2 = new R2(numberOfObjectives, file);
    } else 
      this.r2 = null;

  }

  /**
   * Calculates the hypervolume for the given front. This method uses the internal
   * population as basis for assessing the min and max values in the normalization.
   * 
   * @param frontPath path of the Pareto front to be evaluated.
   * @return the hypervolume value for the given Pareto front.
   */
  @Override
  public double execute(String frontPath) {
    return execute(metricsUtil.readNonDominatedSolutionSet(frontPath));
  }

  /**
   * Calculates the hypervolume for the given front. This method uses the internal
   * population as basis for assessing the min and max values in the normalization.
   * 
   * @param front Pareto front to be evaluated.
   * @return the hypervolume value for the given Pareto front.
   */
  @Override
  public double execute(SolutionSet front) {
    if (internalPopulation.size() != 0) {
      double[] maximumValues = metricsUtil.getMaximumValues(internalPopulation.writeObjectivesToMatrix(), numberOfObjectives);
      for (int i = 0; i < maximumValues.length; i++) {
        //Adding an offset to properly calculate the hypervolume reference point
        maximumValues[i] = maximumValues[i] + offset;
      }
      double[] minimumValues = metricsUtil.getMinimumValues(internalPopulation.writeObjectivesToMatrix(), numberOfObjectives);
      return hypervolume.hypervolume(front.writeObjectivesToMatrix(), maximumValues, minimumValues);
    }
    return 0D;
  }

  public double executeR2(String frontPath) {
    return executeR2(metricsUtil.readNonDominatedSolutionSet(frontPath));
  }

  public double executeR2 (SolutionSet front) {
    if (internalPopulation.size() != 0) {
      return r2.r2(front.writeObjectivesToMatrix(), internalPopulation.writeObjectivesToMatrix());
    }
    return 0D;
  }

}
