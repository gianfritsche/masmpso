#!/bin/bash

# two layer weight vector generation

# m=2
# h1=96
# h2=0
# t=0.5

# m=3
# h1=12
# h2=0
# t=0.5

# m=5
# h1=6
# h2=0
# t=0.5

# m=15
# h1=2
# h2=1
# t=0.5

m=20
h1=2
h2=1
t=0.5

javac WeightVectorsGenerator.java

if [ "$?" = "0" ]; then
	#1 layer generation
	java WeightVectorsGenerator -h $h1 -m $m -t 1.0 > first_layer.dat

	#2 layer generation
	java WeightVectorsGenerator -h $h2 -m $m -t $t > second_layer.dat

	a=$(wc -l < first_layer.dat)
	b=$(wc -l < second_layer.dat)
	
	cat first_layer.dat > $m"m.dat"
	cat second_layer.dat >> $m"m.dat"

	echo $(($a+$b)) > $m.dat
	cat $m"m.dat" >> $m.dat

else
	echo "COMPILATION ERROR!"
fi

echo "done."
