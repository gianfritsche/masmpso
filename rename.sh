#!/bin/bash

# rename.sh

	# . configurationFiles/ACFwithNormalization.conf

	# IFS=',' read -ra mm                   <<< "$mm"
	# IFS=',' read -ra iterations           <<< "$iterations"
	# IFS=',' read -ra sizes                <<< "$sizes"
	# #IFS=',' read -ra problemList          <<< "$problemList"
	# IFS=',' read -ra algorithmsToEvaluate <<< "$algorithmsToEvaluate"
	# IFS=',' read -ra indicatorList        <<< "$indicatorList"
	
	# for ((i=0; i<${#mm[@]}; ++i )) ;do
	# 	m=${mm[i]}
	# 	rsync -vauzP experiment/$m/data/ACFNFSFIXED/ SMPSOStudy/$m/data/ACFNFSFIXED/
	# done


	for i in 2 3 5 10 15 20; do
		rsync -ah --info=progress2 SMPSOStudy/100/100/$i/data/$1/ SMPSOStudy/100/100/$i/data/$1old/
	done	