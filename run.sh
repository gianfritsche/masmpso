#!/bin/bash

# run.sh

dependencies=".:lib/commons-math3-3.5/commons-math3-3.5.jar"
javac_options="-J-XX:MaxHeapSize=256m -J-Xmx512m"
java_options="-XX:MaxHeapSize=256m -Xmx512m"

function log () {
	echo -e "\e[1;34m $1 \e[0m"
}

function error () {
	echo -e "\e[1;31m ERROR: $1 \e[0m"	
}

function checkfordependencies () {
	echo "checking for dependencies"
	if [ ! -d "lib" ];
	then
		echo "making lib directory"
		mkdir lib
	fi
	if [ ! -f "lib/commons-math3-3.5/commons-math3-3.5.jar" ];
	then
		echo "downloading commons match dependency"
		cd lib
		wget "http://mirror.cogentco.com/pub/apache//commons/math/binaries/commons-math3-3.5-bin.tar.gz"
		tar -zxvf "commons-math3-3.5-bin.tar.gz"
		cd ..
	fi
}

function compile () {
	checkfordependencies
	echo "fixing non ASCII chars on jmetal..."
	export JAVA_TOOL_OPTIONS=-Dfile.encoding=UTF8
	echo "loading java files..."
	find jmetal/ org/ test/ -name "*.java" > sources.aux
	echo "compiling..."
	javac $javac_options -classpath $dependencies @sources.aux
} # compile

function load_configuration () {

	log "loading experiment configuration..."
	if [ $# -eq 0 ]; then # if has no arguments
		. configurationFiles/experiment.conf
	else
		. $1
	fi

	IFS=',' read -ra mm                   <<< "$mm"
	IFS=',' read -ra iterations           <<< "$iterations"
	IFS=',' read -ra sizes                <<< "$sizes"
	#IFS=',' read -ra indicatorList        <<< "$indicatorList"
	IFS=',' read -ra problems             <<< "$problemList"
	

	
	# if not set use default
	if [ -z ${mean+x} ]; then 
		echo mean=true   > configurationFiles/AdaptiveChoiceFunction.conf
	else
		echo mean=$mean  > configurationFiles/AdaptiveChoiceFunction.conf
	fi
	if [ -z ${sf+x} ]; then
		echo SF=0.5     >> configurationFiles/AdaptiveChoiceFunction.conf
	else
		echo SF=$sf     >> configurationFiles/AdaptiveChoiceFunction.conf
	fi
	if [ -z ${norm+x} ]; then
		echo norm=true  >> configurationFiles/AdaptiveChoiceFunction.conf
	else
		echo norm=$norm >> configurationFiles/AdaptiveChoiceFunction.conf
	fi

}

function run_remote () {
	error "run_remote: Not implemented yet!"
}

function run_multi () {
	error "run_multi: Not implemented yet!"
}

function run_local () {

	load_configuration $1

	rm experiment.err
	rm experiment.out

	log "running..."

	for ((i=0; i<${#mm[@]}; ++i )); do
	
		echo "[$((i+1))/${#mm[@]}]"
	
		echo iterations=${iterations[i]} > configurationFiles/eval.conf
		echo size=${sizes[i]}           >> configurationFiles/eval.conf
		echo m=${mm[i]}                 >> configurationFiles/eval.conf
		echo runs=$runs                 >> configurationFiles/eval.conf
		echo problems=$problemList      >> configurationFiles/eval.conf
		echo run=$algorithmsToRun       >> configurationFiles/eval.conf

		java $java_options -classpath $dependencies jmetal.experiments.studies.SMPSOStudy r configurationFiles/eval.conf 1> experiment.out 2>> experiment.err

	done	

}

function evaluate () {
	
	load_configuration $1

	rm experiment.err
	rm experiment.out

	log "Generating quality indicators..."

	for ((i=0; i<${#mm[@]}; ++i )) ;do

		echo "[$((i+1))/${#mm[@]}]"

		for ((j=0; j<${#problems[@]}; ++j )) ;do

			echo iterations=${iterations[i]}           > configurationFiles/eval.conf
			echo size=${sizes[i]}                     >> configurationFiles/eval.conf
			echo m=${mm[i]}                           >> configurationFiles/eval.conf
			echo runs=$runs                           >> configurationFiles/eval.conf
			echo problems=${problems[j]}                 >> configurationFiles/eval.conf
			echo run=$algorithmsToRun                 >> configurationFiles/eval.conf
			echo evaluate=$algorithmsToEvaluate       >> configurationFiles/eval.conf
			echo fronts="${problems[j]}_${mm[i]}.ref" >> configurationFiles/eval.conf
			echo indicators=$indicatorList			  >> configurationFiles/eval.conf

			java $java_options -classpath $dependencies jmetal.experiments.studies.SMPSOStudy i configurationFiles/eval.conf 1>> experiment.out 2>> experiment.err

		done
	done

	log "Generating latex report..."

	for ((i=0; i<${#mm[@]}; ++i )) ;do

			echo "[$((i+1))/${#mm[@]}]"

			echo iterations=${iterations[i]}           > configurationFiles/eval.conf
			echo size=${sizes[i]}                     >> configurationFiles/eval.conf
			echo m=${mm[i]}                           >> configurationFiles/eval.conf
			echo runs=$runs                           >> configurationFiles/eval.conf
			echo problems=$problemList                >> configurationFiles/eval.conf
			echo run=$algorithmsToRun                 >> configurationFiles/eval.conf
			echo evaluate=$algorithmsToEvaluate       >> configurationFiles/eval.conf
			echo fronts="${problems[j]}_${mm[i]}.ref" >> configurationFiles/eval.conf
			echo indicators=$indicatorList			  >> configurationFiles/eval.conf

			java $java_options -classpath $dependencies jmetal.experiments.studies.SMPSOStudy l configurationFiles/eval.conf 1>> experiment.out 2>> experiment.err

	done
	
}

# begin main
clear
compile
a=$?
echo "removing aux files..."
rm *.aux

if ! [ $a -eq 0 ]; then
	error "\e[1;31m COMPILATION ERROR! \e[0m"
else 
	log "\e[1;34m COMPILATION SUCCESS! \e[0m"
	if ! [ "$1" == "-co" ]; then # if not compile only
		if [ "$1" == "remote" ]; then
			time run_remote 
		elif [ "$1" == "multi" ]; then
			time run_multi # run over multi servers
		elif [ "$1" == "eval" ]; then
			time evaluate $2
		else
			time run_local $2
		fi
	fi
fi

echo "done."
# end main
