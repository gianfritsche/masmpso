#!/bin/bash

## usage
# compile and run
# ./makefile 
# compile only
# ./makefile -co

dependencies=".:lib/commons-math3-3.5/commons-math3-3.5.jar"
javac_options="-J-XX:MaxHeapSize=256m -J-Xmx512m"
java_options="-XX:MaxHeapSize=256m -Xmx512m"

function log () {
	echo -e "\e[1;34m $1 \e[0m"
}

function checkfordependencies () {
	echo "checking for dependencies"
	if [ ! -d "lib" ];
	then
		echo "making lib directory"
		mkdir lib
	fi
	if [ ! -f "lib/commons-math3-3.5/commons-math3-3.5.jar" ];
	then
		echo "downloading commons match dependency"
		cd lib
		wget "http://mirror.cogentco.com/pub/apache//commons/math/binaries/commons-math3-3.5-bin.tar.gz"
		tar -zxvf "commons-math3-3.5-bin.tar.gz"
		cd ..
	fi
}

function compile () {
	checkfordependencies
	echo "fixing non ASCII chars on jmetal..."
	export JAVA_TOOL_OPTIONS=-Dfile.encoding=UTF8
	echo "loading java files..."
	find jmetal/ org/ -name "*.java" > sources.aux
	echo "compiling..."
	javac $javac_options -classpath $dependencies @sources.aux
} # compile

function smpso_study (){
	echo "SMPSO Study"
	
	if [ $# -eq 0 ]; then
		java $java_options -classpath $dependencies jmetal.experiments.studies.SMPSOStudy ril 1> experiment.out 2> experiment.err
	else
		java $java_options -classpath $dependencies jmetal.experiments.studies.SMPSOStudy $1 1> experiment.out 2> experiment.err
	fi

	# echo "generating plots..."
	# octave statistical_equivalence_plot.m
	
	# echo "generating pdf report..."
	# cd SMPSOStudy/report
	# latex bare_conf.tex 
	# dvipdf bare_conf.dvi
}

function test () {
	echo "Testing..."
	java $java_options -classpath $dependencies jmetal.metaheuristics.smpso.HHSMPSO_main DTLZ1 Real 8 1> experiment.out 2> experiment.err
}

function acf_analysis () {
	echo "Adaptive Choice Function Analysis"

	# for problem in WFG1 WFG6 DTLZ1 DTLZ5; do

	for sf in 0.5 1.0 0.1; do

		echo mean=true > configurationFiles/AdaptiveChoiceFunction.conf
		echo SF=$sf >> configurationFiles/AdaptiveChoiceFunction.conf

		java $java_options -classpath $dependencies jmetal.experiments.studies.SMPSOStudy r 1> experiment.out 2> experiment.err
	
		for problem in DTLZ1 DTLZ3 DTLZ5 WFG1 WFG2 WFG6 WFG7; do

			echo $problem\_$sf
		
			for obj in 3 8 15; do
			
				cd octave/
				mkdir $problem
				./choice_function_analysis.m $problem $obj
				mkdir -p ../SMPSOStudy/report/img/$sf/$obj/$problem
				cp -r $problem/ ../SMPSOStudy/report/img/$sf/$obj/
				cd ..

			done
		done
	done

	echo mean=true > configurationFiles/AdaptiveChoiceFunction.conf
	echo SF=0.5 >> configurationFiles/AdaptiveChoiceFunction.conf

}

function sf_configuration () {
	echo "ACF Scale Factor (SF) configuration..."

	for sf in 0.05 0.5; do
	#for sf in 5.0 0.05 0.5; do

		echo mean=true > configurationFiles/AdaptiveChoiceFunction.conf
		echo SF=$sf >> configurationFiles/AdaptiveChoiceFunction.conf

		java $java_options -classpath $dependencies jmetal.experiments.studies.SMPSOStudy r 1> experiment.out 2> experiment.err
	
		for obj in 2 3 5 8 10 15 20; do
			echo "clean SMPSOStudy/$obj/data/ACFFIXED$sf/"
			rm -r SMPSOStudy/$obj/data/ACFFIXED$sf/
			echo "rename ACFFIXED to ACFFIXED$sf"
			mv SMPSOStudy/$obj/data/ACFFIXED/ SMPSOStudy/$obj/data/ACFFIXED$sf/
		done

	done

	# set back default configuration
	echo mean=true > configurationFiles/AdaptiveChoiceFunction.conf
	echo SF=0.5 >> configurationFiles/AdaptiveChoiceFunction.conf

	java $java_options -classpath $dependencies jmetal.experiments.studies.SMPSOStudy il 1> experiment.out 2> experiment.err
	
}

function run () {
	
	# run experiments over UFPR servers
	# without exceed 2GB quota

	## before run
	# 1 - Install the public key in remote server
	# ssh-copy-id -i $HOME/.ssh/id_rsa.pub <user>@<server>

	# NOTE:
	# rsync
	#	-a: all
	#	-z: compress
	#	-q: quiet
	#	-u: update
	#	-P: partial progress

	name=bowmore
	user=gmfritsche
	server=$name.c3sl.ufpr.br

	log "loading experiment configuration..."
	. configurationFiles/experiment.conf

	log "creating jar file..."
	find -name "*.class" > sources.aux
	jar cf jmetal.jar @sources.aux

	log "cleaning experiment folder..."
	ssh -o StrictHostKeyChecking=no -l $user $server "rm -r $name/experiment"
	ssh -o StrictHostKeyChecking=no -l $user $server "mkdir $name/experiment"

	log "sending jar and configuration files..."
	rsync -azquP jmetal.jar referencePoints/ weightVectors/ $user@$server:~/$name/experiment

	IFS=',' read -ra mm                   <<< "$mm"
	IFS=',' read -ra iterations           <<< "$iterations"
	IFS=',' read -ra sizes                <<< "$sizes"
	#IFS=',' read -ra problemList          <<< "$problemList"
	IFS=',' read -ra algorithmsToEvaluate <<< "$algorithmsToEvaluate"
	IFS=',' read -ra indicatorList        <<< "$indicatorList"	
	#IFS=',' read -ra fronts              <<< "$fronts"
	
	echo mean=true   > configurationFiles/AdaptiveChoiceFunction.conf
	echo SF=0.5     >> configurationFiles/AdaptiveChoiceFunction.conf
	echo norm=false >> configurationFiles/AdaptiveChoiceFunction.conf

	for ((i=0; i<${#mm[@]}; ++i )) ;do

		echo iterations=${iterations[i]} > configurationFiles/SMPSOStudy.conf
		echo size=${sizes[i]}           >> configurationFiles/SMPSOStudy.conf
		echo m=${mm[i]}                 >> configurationFiles/SMPSOStudy.conf
		echo runs=$runs                 >> configurationFiles/SMPSOStudy.conf
		echo problems=$problemList      >> configurationFiles/SMPSOStudy.conf
		echo run=$algorithmsToRun       >> configurationFiles/SMPSOStudy.conf

		log "cleaning configurationFiles and output folders..."
		ssh -o StrictHostKeyChecking=no -l $user $server "rm -r $name/experiment/SMPSOStudy/"
		ssh -o StrictHostKeyChecking=no -l $user $server "rm -r $name/experiment/configurationFiles/"

		log "sending new configurations..."
		rsync -azquP configurationFiles/ $user@$server:~/$name/experiment

		script="cd $name/experiment && java $java_options -cp jmetal.jar jmetal.experiments.studies.SMPSOStudy ril configurationFiles/SMPSOStudy.conf"
		ssh -o StrictHostKeyChecking=no -l $user $server $script

		mkdir -p $name/experiment/$sf
		rsync -azquP $user@$server:~/$name/experiment/SMPSOStudy $name/experiment/$sf

	done

}

# begin main
clear
compile
a=$?
echo "removing aux files..."
rm *.aux

if ! [ $a -eq 0 ]; then
	echo -e "\e[1;31m COMPILATION ERROR! \e[0m"
else 
	echo -e "\e[1;34m COMPILATION SUCCESS! \e[0m"
	if ! [ "$1" == "-co" ]; then # if not compile only
		if [ "$1" == "acf" ]; then
			echo "running acf analysis..."
			acf_analysis
		elif [ "$1" == "test" ]; then
			test
		elif [ "$1" == "sf" ]; then
			sf_configuration
		elif [ "$1" == "run" ]; then
			run
		else # ril (r)un (i)ndicators (l)atex
			echo "running smpso study..."
			smpso_study
		fi
	fi
fi

echo "done."
# end main
