// EqualSolutionTest.java

 /**
 * 
 * Author:
 *    Gian M. Fritsche <gmfritsche@inf.ufpr.br>
 * 
 * 
 * Copyright (c) 2015 Gian M. Fritsche
 * 
 * Note: This is a free software developed based on the open source project 
 * jMetal<http://jmetal.sourceforge.net>. The copy right of jMetal belongs to 
 * its original authors, Antonio J. Nebro and Juan J. Durillo. Nevertheless, 
 * this current version can be redistributed and/or modified under the terms of 
 * the GNU Lesser General Public License as published by the Free Software 
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package test;

import jmetal.core.Solution;
import jmetal.util.comparators.EqualSolutions;
import jmetal.util.comparators.DominanceComparator;

public class EqualSolutionTest {

	public static void test(Solution s1, Solution s2) {
		System.out.println("new: "+s1);
		System.out.println("old: "+s2);
		DominanceComparator dominance_ = new DominanceComparator();
		EqualSolutions equals_ = new EqualSolutions();

	  int flag = dominance_.compare(s1,s2);	
	  if (equals_.compare(s2,s1)==0) {
        System.out.println("NEW and OLD solutions are equal");
      } else if (flag == 1) {        
        System.out.println("The NEW solution is dominated");      
      } else if (flag == -1) {
        System.out.println("The OLD solution is dominated"); 
      } else {
		System.out.println("NEW and OLD non-dominate each other and are different");
      }
	}

	public static void main(String[] args) {
		Solution s1 = new Solution(2);
		s1.setObjective(0,1.0);
		s1.setObjective(1,0.0);

		Solution s2 = new Solution(2);
		s2.setObjective(0,0.0);
		s2.setObjective(1,1.0);

		test(s1,s2);
		
		s1 = new Solution(2);
		s1.setObjective(0,0.5);
		s1.setObjective(1,0.5);

		s2 = new Solution(2);
		s2.setObjective(0,1.0);
		s2.setObjective(1,0.5);

		test(s1,s2);

		test(s2,s1);

		s1 = new Solution(2);
		s1.setObjective(0,0.5);
		s1.setObjective(1,0.5);

		s2 = new Solution(2);
		s2.setObjective(0,0.5);
		s2.setObjective(1,0.5);

		test(s1,s2);

	}
}
