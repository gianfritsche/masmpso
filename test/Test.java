package test;

public class Test {

	public static void main(String[] args) {
		double ideal = 1e+30;
		double nadir = -1e+30;
		System.out.println("ideal: "+ideal);
		System.out.println("nadir: "+nadir);
		System.out.println(Double.MAX_VALUE);
		System.out.println(Double.MAX_VALUE*-1);
		System.out.println(Double.POSITIVE_INFINITY);
		System.out.println(Double.NEGATIVE_INFINITY);
	}

}