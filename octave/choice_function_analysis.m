#! /usr/bin/octave -qf

arg_list = argv ();

problem = arg_list{1};
obj = arg_list{2};
runs=20;
archivers = {'CD', 'MGA', 'Ideal', 'DD'};

stats = {'cf', 'exploitation', 'exploration', 'f1', 'f2', 'f3', 'count'};

for i = 1:length(stats)
	data = load(['../SMPSOStudy/' num2str(obj) '/data/ACFFIXED/' problem '/run0/archiver/' stats{i} '.stat' ]);
	for j = 1:runs-1
		data = data + load(['../SMPSOStudy/' num2str(obj) '/data/ACFFIXED/' problem '/run' num2str(j) '/archiver/' stats{i} '.stat' ]);
	end
	data = data ./ runs;
	plot(data, 'linewidth', 1);
	legend(archivers);
	legend location eastoutside;
	xlabel ('Execution');
	ylabel (stats{i});
	print ([problem '/' stats{i} '.eps'], '-depsc2');
end
data = load(['../SMPSOStudy/' num2str(obj) '/data/ACFFIXED/' problem '/run0/' stats{i} '.stat' ]);
for j = 1:runs-1
	data = data + load(['../SMPSOStudy/' num2str(obj) '/data/ACFFIXED/' problem '/run' num2str(j) '/' stats{i} '.stat' ]);
end
data = data ./ runs;
plot(data, 'linewidth', 1);
legend('fitness');
legend location eastoutside;
xlabel ('Execution');
ylabel ('fitness');
print ([problem '/fitness.eps'], '-depsc2')

