% visualize_dtlz_reference_points.m

in=['../referencePoints/DTLZ1_2.ref'];
dtlz1_2 = load(in);
figure 1
plot(dtlz1_2(:,1), dtlz1_2(:,2), '.')
print -depsc2 dtlz1_2.eps

in=['../referencePoints/DTLZ2-4_2.ref'];
dtlz24_2 = load(in);
figure 1
plot(dtlz24_2(:,1), dtlz24_2(:,2), '.')
print -depsc2 dtlz24_2.eps


in=['../referencePoints/DTLZ1_3.ref'];
dtlz1_3 = load(in);
figure 1
plot3(dtlz1_3(:,1), dtlz1_3(:,2), dtlz1_3(:,3), '.')
print -depsc2 dtlz1_3.eps

in=['../referencePoints/DTLZ2-4_3.ref'];
dtlz24_3 = load(in);
figure 1
plot3(dtlz24_3(:,1), dtlz24_3(:,2), dtlz24_3(:,3), '.')
print -depsc2 dtlz24_3.eps