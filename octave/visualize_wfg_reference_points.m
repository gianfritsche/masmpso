% visualize_WFG_reference_points.m

in=['../referencePoints/WFG3_2.ref'];
WFG3_2 = load(in);
figure 1;
plot(WFG3_2(:,1), WFG3_2(:,2), '.');
print -depsc2 WFG3_2.eps;

% in=['../referencePoints/WFG3_3.ref'];
% WFG3_3 = load(in);
% figure 1;
% plot3(WFG3_3(:,1), WFG3_3(:,2), WFG3_3(:,3), '.');
% print -depsc2 WFG3_3.eps;

