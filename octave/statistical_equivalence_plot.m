% statistical_equivalence_plot.m

algorithms = {"RANDOM-RANDOM", "CD-CD", "CD-NWSUM", "CD-SIGMA", "DD-CD", "DD-NWSUM", "DD-SIGMA" , "IDEAL-CD", "IDEAL-NWSUM", "IDEAL-SIGMA" , "MGA-CD", "MGA-NWSUM", "MGA-SIGMA"}

algs = length(algorithms)

obj = {2, 3, 5, 10, 15}
ind = 'R2'
figure 1
hold on
values = zeros(algs, length(obj))
for j = 1:length(obj)
	bin = load(['../SMPSOStudy/' num2str(obj{j}) '/latex/' ind '.bin'])
	values(:,j) = sum(bin')'
end
color='brgkmcy' 
marker='+o*xsdph'
for i = 1:algs
	plot(values(i,:), [sprintf( '%s', color( mod(i, length(color) ) +1 ) ) '-' sprintf( '%s', marker( mod(i, length(marker) ) +1 ) )],  "linewidth", 1)
	legend(algorithms);
	legend location eastoutside
	xlabel ("objectives");
	ylabel ("equivalences");
end
print -depsc2 R2.eps

obj = {2, 3, 5, 10, 15}
ind = 'IGD'
figure 2
hold on
values = zeros(algs, length(obj))
for j = 1:length(obj)
	bin = load(['../SMPSOStudy/' num2str(obj{j}) '/latex/' ind '.bin'])
	values(:,j) = sum(bin')'
end
color='brgkmcy' 
marker='+o*xsdph'
for i = 1:algs
	plot(values(i,:), [sprintf( '%s', color( mod(i, length(color) ) +1 ) ) '-' sprintf( '%s', marker( mod(i, length(marker) ) +1 ) )],  "linewidth", 1)
	legend(algorithms);
	legend location eastoutside
	xlabel ("objectives");
	ylabel ("equivalences");
end
print -depsc2 IGD.eps

obj = {2, 3, 5}
ind = 'HV'
figure 3
hold on
values = zeros(algs, length(obj))
for j = 1:length(obj)
	bin = load(['../SMPSOStudy/' num2str(obj{j}) '/latex/' ind '.bin'])
	values(:,j) = sum(bin')'
end
color='brgkmcy' 
marker='+o*xsdph'
for i = 1:algs
	plot(values(i,:), [sprintf( '%s', color( mod(i, length(color) ) +1 ) ) '-' sprintf( '%s', marker( mod(i, length(marker) ) +1 ) )],  "linewidth", 1)
	legend(algorithms);
	legend location eastoutside
	xlabel ("objectives");
	ylabel ("equivalences");
end
print -depsc2 HV.eps