#! /usr/bin/octave -qf

# generic_acf_analysis.m

probs = { 'DTLZ1', 'DTLZ3', 'DTLZ5', 'WFG1', 'WFG2', 'WFG6', 'WFG7'};
objs = {'3', '8', '15'};
algs = {'CDCD', 'MGACD', 'IDEALCD', 'DDCD'};

for p = 1:length(probs)
	for o = 1:length(objs)
		a=1
		data = load(['../SMPSOStudy/' objs{o} '/data/' algs{a} '/' probs{p} '/run0/fitness.dat']);
		for r = 1:19
			data = horzcat(data, load(['../SMPSOStudy/' objs{o} '/data/' algs{a} '/' probs{p} '/run' num2str(r) '/fitness.dat']));
		end
		data = mean(data');
		analisys = data;		
		for a = 2:length(algs)
			data = load(['../SMPSOStudy/' objs{o} '/data/' algs{a} '/' probs{p} '/run0/fitness.dat']);
			for r = 1:19
				data = horzcat(data, load(['../SMPSOStudy/' objs{o} '/data/' algs{a} '/' probs{p} '/run' num2str(r) '/fitness.dat']));
			end
			data = mean(data');
			analisys = vertcat(analisys, data);
		end
		plot(analisys', 'linewidth', 1);
		legend(algs);
		legend location eastoutside
		xlabel ('Execution');
		ylabel ('fitness');
		mkdir ([probs{p} '/' objs{o}])
		print ([probs{p} '/' objs{o} '/fitness.eps'], '-depsc2')
	end
end

