//  SMPSODD_Settings.java 
//
//  Authors:
//       Gian Mauricio Fritsche <gmfritsche@inf.ufpr.br>
//
//  Copyright (c) 2015 Gian M. Fritsche
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package jmetal.experiments.settings;

import java.util.HashMap;
import jmetal.core.Algorithm;
import jmetal.core.Problem;
import jmetal.experiments.Settings;
import jmetal.metaheuristics.smpso.SMPSODD;
import jmetal.operators.mutation.Mutation;
import jmetal.operators.mutation.MutationFactory;
import jmetal.util.JMException;
import jmetal.util.parallel.IParallelEvaluator;
import jmetal.util.parallel.MultithreadedEvaluator;


public class SMPSODD_Settings extends Settings {
  public int swarmSize;
  public int archiveSize;
  public int maxIterations;

  /**
   * Constructor
   */
  public SMPSODD_Settings(Problem problem, int size) {
    super(problem.getName()) ;

    problem_ = problem;
    // Default experiments.settings
    swarmSize              = size ;
    archiveSize              = size ;
    maxIterations              = 100 ;
  } // SMPSODD_Settings


  /**
   * Configure SMPSODD with default parameter experiments.settings
   * @return A SMPSODD algorithm object
   * @throws jmetal.util.JMException
   */
  public Algorithm configure() throws JMException {
    Algorithm algorithm ;

    int threads = 0 ; // 0 - use all the available cores
    //int threads = 4 ; 
    IParallelEvaluator parallelEvaluator = new MultithreadedEvaluator(threads) ;

    algorithm = new SMPSODD(problem_, parallelEvaluator) ;
    
    HashMap  parameters ; // Operator parameters
    // Algorithm parameters
    algorithm.setInputParameter("swarmSize",swarmSize);
    algorithm.setInputParameter("archiveSize",archiveSize);
    algorithm.setInputParameter("maxIterations",maxIterations);

    parameters = new HashMap() ;
    parameters.put("probability", 1.0/problem_.getNumberOfVariables()) ;
    parameters.put("distributionIndex", 20.0) ;
    Mutation  mutation = MutationFactory.getMutationOperator("PolynomialMutation", parameters);                    

    algorithm.addOperator("mutation", mutation);

    return algorithm ;
  } // configure

} // SMPSODD_Settings
