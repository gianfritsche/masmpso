//  SMPSOStudy.java
//
//  Author:
//       Gian M. Fritsche
////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package jmetal.experiments.studies;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Map;
import java.util.StringTokenizer;
import java.lang.InterruptedException;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.util.Properties;

import jmetal.core.Algorithm;
import jmetal.core.Problem;
import jmetal.experiments.Experiment;
import jmetal.experiments.Settings;
import jmetal.experiments.settings.GenericSMPSO_Settings;
import jmetal.experiments.settings.HHSMPSO_Settings;
import jmetal.experiments.settings.GenerationalHHSMPSO_Settings;
import jmetal.experiments.settings.MOEADD_Settings;
// import jmetal.experiments.settings.SMPSODDSteadyState_Settings;
import jmetal.experiments.util.Friedman;
import jmetal.util.JMException;
import jmetal.metaheuristics.smpso.SMPSODD;
import jmetal.problems.DTLZ.*;
import jmetal.problems.WFG.*;
import jmetal.util.parallel.IParallelEvaluator;
import jmetal.util.parallel.MultithreadedEvaluator;

import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;

import org.uma.jmetal.statistics.KruskalWallisTest;

import jmetal.metaheuristics.smpso.leaderSelection.LeaderSelectionFactory;
import jmetal.util.archive.ArchiverFactory;
import org.uma.jmetal.hyperheuristic.HighLevelHeuristicFactory;
import jmetal.experiments.settings.MOEAD_DRA_Settings;
import org.uma.jmetal.util.FileUtils;

public class SMPSOStudy extends Experiment {

  int size;
  int iteration;
  // < Indicator < Problem < Algorithm , values > > >
  HashMap<String, HashMap<String, HashMap<String, double[]> > > data;
  // < Indicator < Problem < Algorithm < Algorithm, isDiff > > > >
  HashMap<String, HashMap<String, HashMap<String, HashMap<String, Boolean> > > > statisticalData;
  
  @Override
  public void generateLatexTables() {
    // < Indicator < Problem < Algorithm , value > > >
    HashMap<String, HashMap<String, HashMap<String, Double> > > mean = new HashMap<String, HashMap<String, HashMap<String, Double> > >();
    HashMap<String, HashMap<String, HashMap<String, Double> > > standardDeviation = new HashMap<String, HashMap<String, HashMap<String, Double> > >();
    HashMap<String, HashMap<String, HashMap<String, Boolean> > > bold = new HashMap<String, HashMap<String, HashMap<String, Boolean> > >();
    HashMap<String, HashMap<String, String > > grey = new HashMap<String, HashMap<String, String > >();
    for (Map.Entry<String, HashMap<String, HashMap<String, double[]> > > indicator : data.entrySet()) {
      HashMap<String, HashMap<String, Double> > problemmean = new HashMap<String, HashMap<String, Double> >();
      HashMap<String, HashMap<String, Double> > problemstd = new HashMap<String, HashMap<String, Double> >();
      HashMap<String, HashMap<String, Boolean> > problembold = new HashMap<String, HashMap<String, Boolean> >();
      HashMap<String, String > problemgrey = new HashMap<String, String >();
      for (Map.Entry<String, HashMap<String, double[]> > problem : indicator.getValue().entrySet()) {
        HashMap<String, Double> algorithmmean = new HashMap<String, Double>();
        HashMap<String, Double> algorithmstd = new HashMap<String, Double>();
        HashMap<String, Boolean> algorithmbold = new HashMap<String, Boolean>();
        String sbest = "";
        boolean minimization = indicatorMinimize_.get(indicator.getKey());
        double best = (minimization)?(Double.POSITIVE_INFINITY):(Double.NEGATIVE_INFINITY);
        for (Map.Entry<String, double[]> algorithm : problem.getValue().entrySet()) {
          Mean m = new Mean();
          StandardDeviation std = new StandardDeviation();
          for (double d : algorithm.getValue()) {
            m.increment(d);
            std.increment(d);
          }
          double value = m.getResult();
          algorithmmean.put(algorithm.getKey(), value);
          algorithmstd.put(algorithm.getKey(), std.getResult());
          if ((value < best && minimization ) || (value > best && !minimization )){
            sbest=algorithm.getKey();
            best=value;
          }
        }
        algorithmbold.put(sbest, true);
        HashMap<String, Boolean> test = new HashMap<String, Boolean> ();
        //TEST
        HashMap<String, HashMap<String, HashMap<String, Boolean> > > aux1 = statisticalData.get(indicator.getKey());
        HashMap<String, HashMap<String, Boolean> >  aux2 = aux1.get(problem.getKey());
        HashMap<String, Boolean> aux3 = aux2.get(sbest);
        java.util.Set aux4 = aux3.entrySet();

        for (Map.Entry<String, Boolean> b: statisticalData.get(indicator.getKey()).get(problem.getKey()).get(sbest).entrySet()) {
          algorithmbold.put(b.getKey(), !b.getValue());  
        }
        problemmean.put(problem.getKey(), algorithmmean);
        problemstd.put(problem.getKey(), algorithmstd);
        problembold.put(problem.getKey(), algorithmbold);
        problemgrey.put(problem.getKey(), sbest);
      }
      mean.put(indicator.getKey(), problemmean);
      standardDeviation.put(indicator.getKey(), problemstd);  
      bold.put(indicator.getKey(), problembold);
      grey.put(indicator.getKey(), problemgrey);
    }

    FileWriter os, osbin;
    NumberFormat formatter = new DecimalFormat("0.##E0");
    try {
      for (String i : indicatorList_) {
        FileUtils.checkDirectory(experimentBaseDirectory_+"/latex/");
        os = new FileWriter(experimentBaseDirectory_+"/latex/"+i+".tex");
        osbin = new FileWriter(experimentBaseDirectory_+"/latex/"+i+".bin");
        os.write("\n\\multirow{"+algorithmNameList_.length+"}{*}{"+m+"}"); 
        for (String a : algorithmNameList_) {
          os.write(" & " + a);
          for (String p : problemList_) {

            if (grey.get(i).get(p).equals(a)) {
              os.write(" & \\cellcolor{gray95}");
            } else {
              os.write(" &");
            }

            if (bold.get(i).get(p).get(a)) {
              os.write(" {\\bf " + formatter.format(mean.get(i).get(p).get(a)) + "("+ formatter.format(standardDeviation.get(i).get(p).get(a)) +")}");
              osbin.write(" 1");
            } else {
              os.write(" " + formatter.format(mean.get(i).get(p).get(a)) + "("+ formatter.format(standardDeviation.get(i).get(p).get(a)) +")");  
              osbin.write(" 0");
            }
          }
          osbin.write("\n");
          os.write("\\\\\n");
        }
        os.close();
        osbin.close();
      }

    } catch (IOException ex) {
      ex.printStackTrace();
    }
  
  }

  @Override
  public void generateStatisticalTests (String indicator) {
    HashMap<String, HashMap<String, HashMap<String, Boolean> > > output = new HashMap<String, HashMap<String, HashMap<String, Boolean> > >();  
    HashMap<String, HashMap<String, double[]> > indicatormap = new  HashMap<String, HashMap<String, double[]> > ();
    for (String problem : problemList_ ) {
      HashMap<String, double[]> values = new HashMap<String, double[]>();
      try {
        for (String algorithm : algorithmNameList_ ) {
          ArrayList<Double> d = new ArrayList<Double>();
          FileInputStream fis = new FileInputStream(experimentBaseDirectory_+"/data/"+algorithm+"/"+problem+"/"+indicator);
          InputStreamReader isr = new InputStreamReader(fis);
          BufferedReader br = new BufferedReader(isr);
          String aux = br.readLine(); 
          while (aux != null ) {
            d.add(Double.parseDouble(aux));
            aux = br.readLine();
          }
          double[] dd = new double[d.size()];
          for (int i = 0; i<d.size(); ++i) {
            dd[i] = d.get(i).doubleValue();
          }
          values.put(algorithm, dd);
        }
        output.put(problem, KruskalWallisTest.test(values));
        indicatormap.put(problem, values);
      } catch (IOException | InterruptedException e) {
        e.printStackTrace();
      }
    }

    //// print KruskalWallisTest output
    // for (Map.Entry<String, HashMap<String, HashMap<String, Boolean> > > a : output.entrySet()) {
    //   String p = a.getKey();
    //   HashMap<String, HashMap<String, Boolean> > aux = a.getValue();
    //   System.out.println(p+":"+aux.size()); 
    //   for (Map.Entry<String, HashMap<String, Boolean> > e : aux.entrySet()) {
    //     String key = e.getKey();
    //     HashMap<String, Boolean> value = e.getValue();
    //     System.out.println(key);
    //     for (Map.Entry<String, Boolean> en : value.entrySet()) {
    //       String alg = en.getKey();
    //       Boolean bool = en.getValue();
    //       System.out.print("\t"+alg+":"+bool);
    //     }
    //     System.out.println();
    //   }
    // }

    statisticalData.put(indicator, output);
    data.put(indicator, indicatormap);
  }

  public Algorithm getAlgorithm(String option, Problem problem) throws ClassNotFoundException, JMException {

    String problemName = problem.getName();

    if (option.equals("MOEADDRA")) { 
      return (new MOEAD_DRA_Settings(problem, size, iteration)).configure();
    }

    // GenerationalHHSMPSO
    if (option.equals("FIXED")) { //ARQUIVE FIXED, LEADER_SELECTION ACF
      return (new GenerationalHHSMPSO_Settings(problem, size, HighLevelHeuristicFactory.FIXED, iteration, experimentBaseDirectory_+"/data/"+
      option+"/"+problemName+"/run"+runs)).configure();
    }
    
    if (option.equals("ROULETTE")) { //ARQUIVE FIXED, LEADER_SELECTION ACF
      return (new GenerationalHHSMPSO_Settings(problem, size, HighLevelHeuristicFactory.ROULETTE, iteration, experimentBaseDirectory_+"/data/"+
      option+"/"+problemName+"/run"+runs)).configure();
    }

    if (option.equals("ACF")) { //ARQUIVE ACF, LEADER_SELECTION FIXED
      return (new GenerationalHHSMPSO_Settings(problem, size, HighLevelHeuristicFactory.ACF, iteration, experimentBaseDirectory_+"/data/"+
      option+"/"+problemName+"/run"+runs)).configure();
    }
    if (option.equals("RANDOM")) {
      return (new GenerationalHHSMPSO_Settings(problem, size, HighLevelHeuristicFactory.RANDOM, iteration, experimentBaseDirectory_+"/data/"+
      option+"/"+problemName+"/run"+runs)).configure();
    }
    // HHSMPSO (ARQUIVE LEADER_SELECTION)
    if (option.equals("FIXEDACF")) { //ARQUIVE FIXED, LEADER_SELECTION ACF
      return (new HHSMPSO_Settings(problem, size, HighLevelHeuristicFactory.FIXED, HighLevelHeuristicFactory.ACF, iteration, experimentBaseDirectory_+"/data/"+
      option+"/"+problemName+"/run"+runs)).configure();
    }
    if (option.equals("ACFFIXED")) { //ARQUIVE ACF, LEADER_SELECTION FIXED
      return (new HHSMPSO_Settings(problem, size, HighLevelHeuristicFactory.ACF, HighLevelHeuristicFactory.FIXED, iteration, experimentBaseDirectory_+"/data/"+
      option+"/"+problemName+"/run"+runs)).configure();
    }
    if (option.equals("ACFACF")) {
      return (new HHSMPSO_Settings(problem, size, HighLevelHeuristicFactory.ACF, HighLevelHeuristicFactory.ACF, iteration, experimentBaseDirectory_+"/data/"+
      option+"/"+problemName+"/run"+runs)).configure();
    }
    if (option.equals("RANDOMFIXED")) {
      return (new HHSMPSO_Settings(problem, size, HighLevelHeuristicFactory.RANDOM, HighLevelHeuristicFactory.FIXED, iteration, experimentBaseDirectory_+"/data/"+
      option+"/"+problemName+"/run"+runs)).configure();
    }
    if (option.equals("FIXEDRANDOM")) {
      return (new HHSMPSO_Settings(problem, size, HighLevelHeuristicFactory.FIXED, HighLevelHeuristicFactory.RANDOM, iteration, experimentBaseDirectory_+"/data/"+
      option+"/"+problemName+"/run"+runs)).configure();
    }
    if (option.equals("RANDOMRANDOM")) {
      return (new HHSMPSO_Settings(problem, size, HighLevelHeuristicFactory.RANDOM, HighLevelHeuristicFactory.RANDOM, iteration, experimentBaseDirectory_+"/data/"+
      option+"/"+problemName+"/run"+runs)).configure();
    }

    //GenericSMPSO (ARQUIVE LEADER_SELECTION)
    if (option.equals("CDCD")) {
      return (new GenericSMPSO_Settings(problem, size, ArchiverFactory.CROWDING_ARCHIVE, LeaderSelectionFactory.CROWDING_LEADER_SELECTION, iteration)).setStatPath(experimentBaseDirectory_+"/data/"+
      option+"/"+problemName+"/run"+runs).configure();
    }
    if (option.equals("CDNWSUM")) {
      return  (new GenericSMPSO_Settings(problem, size, ArchiverFactory.CROWDING_ARCHIVE, LeaderSelectionFactory.NWSUM_LEADER_SELECTION, iteration)).setStatPath(experimentBaseDirectory_+"/data/"+
      option+"/"+problemName+"/run"+runs).configure();
    }
    if (option.equals("CDSIGMA")) {
      return (new GenericSMPSO_Settings(problem, size, ArchiverFactory.CROWDING_ARCHIVE, LeaderSelectionFactory.SIGMA_LEADER_SELECTION, iteration)).setStatPath(experimentBaseDirectory_+"/data/"+
      option+"/"+problemName+"/run"+runs).configure();
    }
    if (option.equals("DDCD")) {
      return (new GenericSMPSO_Settings(problem, size, ArchiverFactory.DOMINANCE_AND_DECOMPOSITION_ARCHIVE, LeaderSelectionFactory.CROWDING_LEADER_SELECTION, iteration)).setStatPath(experimentBaseDirectory_+"/data/"+
      option+"/"+problemName+"/run"+runs).configure();
    }
    if (option.equals("DDNWSUM")) {
      return (new GenericSMPSO_Settings(problem, size, ArchiverFactory.DOMINANCE_AND_DECOMPOSITION_ARCHIVE, LeaderSelectionFactory.NWSUM_LEADER_SELECTION, iteration)).setStatPath(experimentBaseDirectory_+"/data/"+
      option+"/"+problemName+"/run"+runs).configure();
    }
    if (option.equals("DDSIGMA")) {
      return (new GenericSMPSO_Settings(problem, size, ArchiverFactory.DOMINANCE_AND_DECOMPOSITION_ARCHIVE, LeaderSelectionFactory.SIGMA_LEADER_SELECTION, iteration)).setStatPath(experimentBaseDirectory_+"/data/"+
      option+"/"+problemName+"/run"+runs).configure();
    }
    if (option.equals("IDEALCD")) {
      return (new GenericSMPSO_Settings(problem, size, ArchiverFactory.IDEAL_ARCHIVE, LeaderSelectionFactory.CROWDING_LEADER_SELECTION, iteration)).setStatPath(experimentBaseDirectory_+"/data/"+
      option+"/"+problemName+"/run"+runs).configure();
    }
    if (option.equals("IDEALNWSUM")) {
      return (new GenericSMPSO_Settings(problem, size, ArchiverFactory.IDEAL_ARCHIVE, LeaderSelectionFactory.NWSUM_LEADER_SELECTION, iteration)).setStatPath(experimentBaseDirectory_+"/data/"+
      option+"/"+problemName+"/run"+runs).configure();
    }
    if (option.equals("IDEALSIGMA")) {
      return (new GenericSMPSO_Settings(problem, size, ArchiverFactory.IDEAL_ARCHIVE, LeaderSelectionFactory.SIGMA_LEADER_SELECTION, iteration)).setStatPath(experimentBaseDirectory_+"/data/"+
      option+"/"+problemName+"/run"+runs).configure();
    }
    if (option.equals("MGACD")) {
      return (new GenericSMPSO_Settings(problem, size, ArchiverFactory.MGA_ARCHIVE, LeaderSelectionFactory.CROWDING_LEADER_SELECTION, iteration)).setStatPath(experimentBaseDirectory_+"/data/"+
      option+"/"+problemName+"/run"+runs).configure();
    }
    if (option.equals("MGANWSUM")) {
      return (new GenericSMPSO_Settings(problem, size, ArchiverFactory.MGA_ARCHIVE, LeaderSelectionFactory.NWSUM_LEADER_SELECTION, iteration)).setStatPath(experimentBaseDirectory_+"/data/"+
      option+"/"+problemName+"/run"+runs).configure();
    }
    if (option.equals("MGASIGMA")) {
      return (new GenericSMPSO_Settings(problem, size, ArchiverFactory.MGA_ARCHIVE, LeaderSelectionFactory.SIGMA_LEADER_SELECTION, iteration)).setStatPath(experimentBaseDirectory_+"/data/"+
      option+"/"+problemName+"/run"+runs).configure();
    }
    if (option.equals("MOEADD")) {
      return (new MOEADD_Settings (problem, size)).configure();
    }
    // default
    throw new ClassNotFoundException(option);
  }


 /**
   * Configures the algorithms in each independent run
   * @param problemName The problem to solve
   * @param problemIndex
   * @param algorithm Array containing the algorithms to run
   * @throws ClassNotFoundException 
   */
  public synchronized void algorithmSettings(String problemName, 
  		                                       int problemIndex, 
  		                                       Algorithm[] algorithm) 
    throws ClassNotFoundException {  	
    	
    int numberOfAlgorithms = algorithmNameList_.length;
  
    IParallelEvaluator parallelEvaluator = new MultithreadedEvaluator(0) ;

    //algorithm[0] = new SMPSODD(new DTLZ1("Real"), parallelEvaluator) ;

    Problem problem = null;
    String type = "Real";

    if (problemName.equals("DTLZ1")) {
      problem = new DTLZ1(type, m+4, m);
    } else if (problemName.equals("DTLZ7")) {
      problem = new DTLZ7(type, m+19, m);
    } else if (problemName.equals("DTLZ2")) {
      problem = new DTLZ2(type, m+9, m);
    } else if (problemName.equals("DTLZ3")) {
      problem = new DTLZ3(type, m+9, m);
    } else if (problemName.equals("DTLZ4")) {
      problem = new DTLZ4(type, m+9, m);
    } else if (problemName.equals("DTLZ5")) {
      problem = new DTLZ5(type, m+9, m);
    } else if (problemName.equals("DTLZ6")) {
      problem = new DTLZ6(type, m+9, m);
    } else if (problemName.equals("WFG1")) {
      problem = new WFG1(type, (2*(m-1)), 20, m);
    } else if (problemName.equals("WFG2")) {
      problem = new WFG2(type, (2*(m-1)), 20, m);
    } else if (problemName.equals("WFG3")) {
      problem = new WFG3(type, (2*(m-1)), 20, m);
    } else if (problemName.equals("WFG4")) {
      problem = new WFG4(type, (2*(m-1)), 20, m);
    } else if (problemName.equals("WFG5")) {
      problem = new WFG5(type, (2*(m-1)), 20, m);
    } else if (problemName.equals("WFG6")) {
      problem = new WFG6(type, (2*(m-1)), 20, m);
    } else if (problemName.equals("WFG7")) {
      problem = new WFG7(type, (2*(m-1)), 20, m);
    } else if (problemName.equals("WFG8")) {
      problem = new WFG8(type, (2*(m-1)), 20, m);
    } else if (problemName.equals("WFG9")) {
      problem = new WFG9(type, (2*(m-1)), 20, m);
    } else {
      throw new ClassNotFoundException(problemName);
    }
    
    try {
      int i = 0;
      for (String alg : algorithmNameList_ ) {
        algorithm[i++] = getAlgorithm(alg, problem);
      }
    } catch (JMException | ClassNotFoundException e) {
      e.printStackTrace();
    }
    

  } // algorithmSettings

  private static boolean initQualityIndicator(String indicator, Experiment exp) {
   
    String sm = Integer.toString(exp.m);

    if (indicator.equals("R2")) {

      // exp.problemList_     = new String[] {
      // "DTLZ1", "DTLZ2", "DTLZ3", "DTLZ4", "DTLZ5", "DTLZ6", "DTLZ7"};

      // exp.paretoFrontFile_ = new String[] {("DTLZ1_"+sm+".ref"),
      // ("DTLZ2-4_"+sm+".ref"), ("DTLZ2-4_"+sm+".ref"), ("DTLZ2-4_"+sm+".ref")
      // , ("DTLZ2-4_"+sm+".ref"), ("DTLZ2-4_s"+sm+".ref"), ("DTLZ2-4_"+sm+".ref")  } ;

      exp.indicatorList_   = new String[] {"R2"};

      return true;
    } 

    if (indicator.equals("IGD")) {

      // exp.problemList_     = new String[] {"DTLZ1", "DTLZ2", "DTLZ3", "DTLZ4"};
      
      // exp.paretoFrontFile_ = new String[] {("DTLZ1_"+sm+".ref"),
      // ("DTLZ2-4_"+sm+".ref"), ("DTLZ2-4_"+sm+".ref"), ("DTLZ2-4_"+sm+".ref")} ;

      exp.indicatorList_   = new String[] {"IGD"};

      return true;
    } 

    if (indicator.equals("HV") && exp.m < 8 ) {

      // exp.problemList_     = new String[] {
      // "DTLZ1", "DTLZ2", "DTLZ3", "DTLZ4", "DTLZ5", "DTLZ6", "DTLZ7"};

      // exp.paretoFrontFile_ = new String[] {("DTLZ1_"+sm+".ref"),
      // ("DTLZ2-4_"+sm+".ref"), ("DTLZ2-4_"+sm+".ref"), ("DTLZ2-4_"+sm+".ref")
      // , ("DTLZ2-4_"+sm+".ref"), ("DTLZ2-4_s"+sm+".ref"), ("DTLZ2-4_"+sm+".ref")  } ;

      exp.indicatorList_   = new String[] {"HV"};

      return true;
    } 

    return false;

  }

	public static void main(String[] args) throws JMException, IOException  {

    int iterations[];
    int sizes[];
    int mm[];
    int runs;
    String[] problemList = null;
    String[] paretoFrontFile = null;
    String[] algorithmsToRun = null;    
    String[] algorithmsToEvaluate = null;
    String[] indicatorList_ = null;


    boolean loadFromFile = args.length > 1;
    String file = args[1];
    Properties configuration = new Properties();

    if (loadFromFile) {
      try {
        InputStreamReader isr = new InputStreamReader(new FileInputStream(file));
        configuration.load(isr);
      } catch (IOException e){
        e.printStackTrace();
      }
      //iterations
      String aux = configuration.getProperty("iterations");
      iterations = new int[1];
      if (aux != null)
        iterations[0] = Integer.parseInt(aux);
      else
        iterations[0] = 100;
      System.out.println("Iterations: "+iterations[0]);
      //size 
      aux = configuration.getProperty("size");
      sizes = new int[1];
      if (aux != null)
        sizes[0] = Integer.parseInt(aux);
      else
        sizes[0] = 100;
      System.out.println("Population and Archive Size: "+sizes[0]);
      //number of objectives
      aux = configuration.getProperty("m");
      mm = new int[1];
      if (aux != null)
        mm[0] = Integer.parseInt(aux);
      else
        mm[0] = 2;
      System.out.println("Number of Objectives: "+mm[0]);
      //independent runs
      aux = configuration.getProperty("runs");
      runs = 30;
      if (aux != null)
        runs = Integer.parseInt(aux);
      System.out.println("Independent runs: "+runs);
      // problem list
      aux = configuration.getProperty("problems");
      problemList = new String[] {"DTLZ1"};
      if (aux != null)
        problemList = aux.split(",");
      // pareto front files
      aux = configuration.getProperty("fronts");
      paretoFrontFile = new String[] {(problemList[0]+"_"+Integer.toString(mm[0])+".ref")};
      if (aux != null)
        paretoFrontFile = aux.split(",");
      // algorithm to run
      aux = configuration.getProperty("run");
      algorithmsToRun = new String[] {"ACFFIXED"};
      if (aux != null)
        algorithmsToRun = aux.split(",");
      // algorithms to evaluate
      aux = configuration.getProperty("evaluate");
      algorithmsToEvaluate = algorithmsToRun;
      if (aux != null)
        algorithmsToEvaluate = aux.split(",");
      // indicators list
      aux = configuration.getProperty("indicators");
      indicatorList_ = new String[] {"R2"};
      if (aux != null)
        indicatorList_ = aux.split(",");

    } else {

      //// ACF Scale Factor (SF) configuration
      iterations = new int[] {100, 150, 250, 400, 500, 750, 1000};
      sizes = new int[] {97, 91, 210, 156, 275, 135, 230};
      mm   = new int[] {2, 3, 5, 8, 10, 15, 20};
      runs = 10;

    }

    for (int i = 0; i < mm.length; ++i) {

      int m = mm[i];

      String sm = Integer.toString(m);

      if (!loadFromFile) {   
        // BEGIN SETTINGS 

        // String indicatorList_ = new String[] {};
        // String[] indicatorList_ = new String[] {"HV", "IGD", "R2"};
        // String[] problemList = new String[] {"WFG1", "WFG2", "WFG3", "WFG4", "WFG5", "WFG6", "WFG7", "WFG8", "WFG9"}; 
        // String[] paretoFrontFile = new String[] {("WFG1_"+sm+".ref"), ("WFG2_"+sm+".ref"), ("WFG3_"+sm+".ref"), ("WFG4_"+sm+".ref"), ("WFG5_"+sm+".ref"), ("WFG6_"+sm+".ref"), ("WFG7_"+sm+".ref"), ("WFG8_"+sm+".ref"), ("WFG9_"+sm+".ref")} ;
        // String[] algorithmsToRun  = new String[] {"ACFFIXED", "MOEADDRA"} ;    
        // String[] algorithmsToEvaluate = new String[] {"ACFFIXED", "MOEADDRA"} ;
        // String[] algorithmsToRun  = new String[]  {"ACFFIXED", "RANDOMFIXED", "CDCD", "MGACD", "IDEALCD", "DDCD"} ;    
        // String[] algorithmsToEvaluate = new String[] {"ACFFIXED", "RANDOMFIXED", "CDCD", "MGACD", "IDEALCD", "DDCD"} ;

        // ACF Scale Factor (SF) configuration
        problemList = new String[] {"DTLZ1", "DTLZ2", "DTLZ3", "DTLZ4", "DTLZ5", "DTLZ6", "DTLZ7"};
        paretoFrontFile = new String[] {("DTLZ1_"+sm+".ref"), ("DTLZ2-4_"+sm+".ref"), ("DTLZ2-4_"+sm+".ref"), ("DTLZ2-4_"+sm+".ref") , ("DTLZ2-4_"+sm+".ref"), ("DTLZ2-4_"+sm+".ref"), ("DTLZ2-4_"+sm+".ref")} ;
        algorithmsToRun  = new String[]  {"ACFFIXED"} ;    
        algorithmsToEvaluate = new String[] {"ACFFIXED5.0", "ACFFIXED0.05", "ACFFIXED0.5"} ;
        indicatorList_ = new String[] {"R2"};

      } // END SETTINGS

  		SMPSOStudy exp = new SMPSOStudy();
  		
      exp.size = sizes[i];
      exp.iteration = iterations[i];
      exp.m = m;
      
      exp.experimentName_  = "SMPSOStudy" ;
  		
      // exp.algorithmNameList_   = new String[] {"RANDOMRANDOM", "CDCD", "CDNWSUM", "CDSIGMA", "DDCD", "DDNWSUM", "DDSIGMA" , "IDEALCD", "IDEALNWSUM", "IDEALSIGMA" , "MGACD", "MGANWSUM", "MGASIGMA"} ;
      //exp.algorithmNameList_   = new String[] {"CDCD", "DDCD", "IDEALCD", "MGACD"} ;
      exp.algorithmNameList_   = algorithmsToRun;

      //// ALL PROBLEMS
      // exp.problemList_     = new String[] {
      //   "DTLZ1", "DTLZ2", "DTLZ3", "DTLZ4" //};  
      //   , "DTLZ5", "DTLZ6", "DTLZ7" // };
      //  ,"WFG1", "WFG2", "WFG3", "WFG4", "WFG5", "WFG6", "WFG7", "WFG8", "WFG9"} ;    

      //// R2 and HV
      // exp.problemList_     = new String[] {
      // "DTLZ1", "DTLZ2", "DTLZ3", "DTLZ4", "DTLZ5", "DTLZ6", "DTLZ7"};
      // exp.paretoFrontFile_ = new String[] {("DTLZ1_"+sm+".ref"),
      // ("DTLZ2-4_"+sm+".ref"), ("DTLZ2-4_"+sm+".ref"), ("DTLZ2-4_"+sm+".ref")
      // , ("DTLZ2-4_"+sm+".ref"), ("DTLZ2-4_s"+sm+".ref"), ("DTLZ2-4_"+sm+".ref")  } ;


      // ACF Analysis
      exp.problemList_  = problemList;
      exp.paretoFrontFile_ = paretoFrontFile;

      //// IGD
      // exp.problemList_     = new String[] {"DTLZ1", "DTLZ2", "DTLZ3", "DTLZ4"};
      // exp.paretoFrontFile_ = new String[] {("DTLZ1_"+sm+".ref"),
      // ("DTLZ2-4_"+sm+".ref"), ("DTLZ2-4_"+sm+".ref"), ("DTLZ2-4_"+sm+".ref")} ;

      // exp.indicatorList_   = new String[] {"IGD"};
      // exp.indicatorList_   = new String[] {"HV"};
      //  exp.indicatorList_   = new String[] {"R2"};

      int numberOfAlgorithms = exp.algorithmNameList_.length ;

      exp.experimentBaseDirectory_ = exp.experimentName_+"/"+exp.iteration+"/"+exp.size+"/"+sm;
      exp.paretoFrontDirectory_ = "referencePoints";
      
      exp.algorithmSettings_ = new Settings[numberOfAlgorithms] ;
      
      exp.independentRuns_ = runs;

      exp.initExperiment();

      // Run the experiments
      int numberOfThreads = 1;
      // int numberOfThreads = Runtime.getRuntime().availableProcessors() - 1;
      if ((args.length > 0 && args[0].contains("r")) || (args.length == 0)) { // if contains run (r) or default
        exp.runExperiment(numberOfThreads);
      }

      exp.algorithmNameList_   = algorithmsToEvaluate;
      
      for (String indicator : indicatorList_) {

        if (initQualityIndicator(indicator, exp)) {

          if ((args.length > 0 && args[0].contains("i")) || (args.length == 0)) { // if contains indicators (i) or default
            exp.generateQualityIndicators() ;
          }
          
          exp.data = new HashMap<String, HashMap<String, HashMap<String, double[]> > > (); 
          exp.statisticalData = new HashMap<String, HashMap<String, HashMap<String, HashMap<String, Boolean> > > > ();   
          
          if ((args.length > 0 && args[0].contains("l")) || (args.length == 0)) { // if contains latex (l) or default
            for (String ind : exp.indicatorList_) {
              exp.generateStatisticalTests(ind);        
            }

            exp.generateLatexTables() ;
          
            // Configure the R scripts to be generated
            int rows  ;
            int columns  ;
            String prefix ;
            String [] problems ;
            rows = 3 ;
            columns = 3 ;
            prefix = new String("Problems");
            problems = exp.problemList_ ;

            boolean notch ;
            exp.generateRBoxplotScripts(rows, columns, problems, prefix, notch = true, exp) ;
            exp.generateRWilcoxonScripts(problems, prefix, exp) ;

            // Applying Friedman test
            Friedman test = new Friedman(exp);
            for (String ind : exp.indicatorList_) {
              test.executeTest(ind);        
            }
          }
        }
      }
  	}
  }
}
