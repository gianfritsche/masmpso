//  PenaltyBasedBoudaryIntersection.java
//
//  Implemented based on dMopso fitnessFunction
//
//  Author:
//       Gian M. Fritsche <gmfritsche@inf.ufpr.br>
//
//  Copyright (c) 2015 Gian M. Fritsche
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package jmetal.qualityIndicator;

import jmetal.core.Solution;

public class PenaltyBasedBoudaryIntersection {

	/**
	* Z vector (ideal point)
	*/
	private double[] z_;

	private int objectives_;

	public PenaltyBasedBoudaryIntersection(int objectives){
		objectives_ = objectives;
		z_ = new double[objectives_];
	    for (int i = 0; i < objectives_; i++) {
	      z_[i] = 1.0e+30;
	    }
	}

	public void updateReference(Solution individual) {
	    for (int n = 0; n < objectives_; n++) {
	      if (individual.getObjective(n) < z_[n]) {
	        z_[n] = individual.getObjective(n);
	      }
	    }
	  } // updateReference

	public double PBI(Solution sol, double[] lambda){
	    double fitness = 0.0;
	            
	    double d1, d2, nl;
	    double theta = 5.0;

	    d1 = d2 = nl = 0.0;
	    
	    for (int i = 0; i < objectives_; i++)
	    {
	      d1 += (sol.getObjective(i) - z_[i]) * lambda[i];
	      nl += Math.pow(lambda[i], 2.0);
	    }     
	    nl = Math.sqrt(nl);
	    d1 = Math.abs(d1) / nl;
	    
	    for (int i = 0; i < objectives_; i++)
	    {
	      d2 += Math.pow((sol.getObjective(i) - z_[i]) - d1 * (lambda[i] / nl), 2.0);
	    }     
	    d2 = Math.sqrt(d2);

	    fitness = (d1 + theta * d2);  

	    return fitness; 
	} // PBI

}