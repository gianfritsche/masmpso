package jmetal.qualityIndicator;

import jmetal.core.SolutionSet;

/**
 * This class was designed to help the calculation of Hypervolume.
 * The idea is to add all available Pareto fronts into an <b>internal
 * population</b>, in order to perform an exact normalization.
 * This internal population is used for assessing the max and min
 * values for the normalization. It is useful when you don't know
 * what are these values and must use the max known and min known
 * values.
 * <br/>
 * <br/>
 * If the intention is to compare the fronts A, B and C, then the
 * addParetoFront() method must be invoked three times giving A, B
 * and then C as input.
 */
public class R2Calculator extends Calculator {

  private final R2 r2;

  private final int numberOfObjectives;
  private final double offset;
  
   /**
   * Constructor for the R2 calculator.
   * 
   * @param numberOfObjectives the number of objectives for the problem.
   *        weight vectors file
   */
  public R2Calculator(int numberOfObjectives, String file ) {
    this(numberOfObjectives, 0.01, file);
  }
  
  public R2Calculator(int numberOfObjectives, double offset, String file) {
    this.numberOfObjectives = numberOfObjectives;
    this.offset = offset;
    this.r2 = new R2(numberOfObjectives, file);
  }

  /**
   * Calculates the hypervolume for the given front. This method uses the internal
   * population as basis for assessing the min and max values in the normalization.
   * 
   * @param frontPath path of the Pareto front to be evaluated.
   * @return the hypervolume value for the given Pareto front.
   */
  @Override
  public double execute(String frontPath) {
    return execute(metricsUtil.readNonDominatedSolutionSet(frontPath));
  }

  /**
   * Calculates the hypervolume for the given front. This method uses the internal
   * population as basis for assessing the min and max values in the normalization.
   * 
   * @param front Pareto front to be evaluated.
   * @return the hypervolume value for the given Pareto front.
   */
  @Override
  public double execute(SolutionSet front) {
    if (internalPopulation.size() != 0) {
      return r2.r2(front.writeObjectivesToMatrix(), internalPopulation.writeObjectivesToMatrix());
    }
    return 0D;
  }

}
