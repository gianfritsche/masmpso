//  ArchiverFactory.java
//
//  Author:
//       Gian Mauricio Fritsche <gmfritsche@inf.ufpr.br>
//
//  Copyright (c) 2015 Gian M. Fritsche
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package jmetal.util.archive;

import jmetal.core.Solution;
import jmetal.util.archive.Archive;
import jmetal.util.archive.CrowdingArchive;
import jmetal.util.archive.IdealArchive;
import jmetal.util.archive.MultiLevelGridArchive;
import jmetal.util.archive.HypervolumeArchive;
import java.lang.ClassNotFoundException;
import java.util.List;
import java.util.ArrayList;

public class ArchiverFactory {

	public static String CROWDING_ARCHIVE						=	"CrowdingArchive";
	public static String HYPERVOLUME_ARCHIVE					=	"HypervolumeArchive";
	public static String DOMINANCE_AND_DECOMPOSITION_ARCHIVE	=	"DominanceAndDecompositionArchive";
	public static String IDEAL_ARCHIVE							=	"IdealArchive";
	public static String MGA_ARCHIVE							=	"MultiLevelGridArchive";

	public ArchiverFactory( ) { }

	public Archive getArchiver (String archive, int maxSize, int numberOfObjectives, List<Solution> solutionList, double[] zp_, double[] nzp_) throws ClassNotFoundException {
		if (archive.equals(CROWDING_ARCHIVE) )
			return new CrowdingArchive(maxSize, numberOfObjectives, solutionList);
		else if (archive.equals(HYPERVOLUME_ARCHIVE) )
			return new HypervolumeArchive(maxSize, numberOfObjectives);
		else if (archive.equals(MGA_ARCHIVE) )
			return new MultiLevelGridArchive(maxSize, numberOfObjectives, solutionList, zp_, nzp_);
		else if (archive.equals(IDEAL_ARCHIVE)) 
			return new IdealArchive(maxSize, numberOfObjectives, solutionList, zp_, nzp_);
		else if (archive.equals(DOMINANCE_AND_DECOMPOSITION_ARCHIVE) )
			return new DominanceAndDecompositionArchive(maxSize, numberOfObjectives, solutionList, zp_, nzp_);
		else
			throw new ClassNotFoundException(archive);
	}

	public Archive getArchiver (String archive, int maxSize, int numberOfObjectives, List<Solution> solutionList) throws ClassNotFoundException {
		
		double[] zp_ = new double[numberOfObjectives];
		double[] nzp_ = new double[numberOfObjectives];

		for (int i = 0; i < numberOfObjectives; i++)
	      	zp_[i] = 1.0e+30;
	    for (int i = 0; i < numberOfObjectives; i++)
		  nzp_[i] = -1.0e+30;

		return (getArchiver(archive, maxSize, numberOfObjectives,solutionList,zp_, nzp_));
	}

	public Archive getArchiver (String archive, int maxSize, int numberOfObjectives) throws ClassNotFoundException {		
		return (getArchiver( archive, maxSize, numberOfObjectives, new ArrayList<Solution>()));
	}

	public Archive getArchiver (String archive, int maxSize, int numberOfObjectives, double[] zp_, double[] nzp_) throws ClassNotFoundException {
		return (getArchiver( archive, maxSize, numberOfObjectives, new ArrayList<Solution>(), zp_, nzp_));
	}

}