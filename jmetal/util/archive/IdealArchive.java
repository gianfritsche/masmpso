/**
 * IdealArchive.java
 * 
 * This is main implementation of Ideal Archiver,
 * based on: 
 *    Britto, A and Pozo, A
 *    "Using archiving methods to control convergence and diversity for Many-Objective Problems in Particle Swarm Optimization"
 *    WCCI 2012 IEEE World Congress on Computational Intelligence
 * 
 * Author:
 *    Gian M. Fritsche <gmfritsche@inf.ufpr.br>
 * 
 * 
 * Copyright (c) 2015 Gian M. Fritsche
 * 
 * Note: This is a free software developed based on the open source project 
 * jMetal<http://jmetal.sourceforge.net>. The copy right of jMetal belongs to 
 * its original authors, Antonio J. Nebro and Juan J. Durillo. Nevertheless, 
 * this current version can be redistributed and/or modified under the terms of 
 * the GNU Lesser General Public License as published by the Free Software 
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package jmetal.util.archive;

import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.util.Distance;
import jmetal.util.comparators.DominanceComparator;
import jmetal.util.comparators.IdealComparator;
import jmetal.util.comparators.EqualSolutions;
import java.util.Comparator;
import java.util.List;

public class IdealArchive extends Archive {

	private double[] zp_;   // ideal point for Pareto-based population
	private double[] nzp_;  // nadir point for Pareto-based population

	/** 
	* Stores the maximum size of the archive.
	*/
	private int maxSize_;

	/**
	* stores the number of the objectives.
	*/
	private int objectives_;    

	/**
	* Stores a <code>Comparator</code> for dominance checking.
	*/
	private Comparator dominance_;

	/**
	* Stores a <code>Comparator</code> for equality checking (in the objective
	* space).
	*/
	private Comparator equals_; 

	private Comparator idealComparator;

	/**
	* Stores a <code>Distance</code> object, for distances utilities
	*/
	private Distance distance_;


	public IdealArchive (int maxSize, int objectives_, List<Solution> solutionList, double[] zp_, double[] nzp_) {
		this(maxSize, objectives_, solutionList);
		this.zp_ = zp_;
		this.nzp_ = nzp_;
	}

	public IdealArchive(int maxSize, int objectives_, List<Solution> solutionList) {
		this(maxSize, objectives_);
		super.solutionsList_ = solutionList;
	}

	public IdealArchive(int maxSize, int numberOfObjectives) {
	    super(maxSize);
	    maxSize_          = maxSize;
	    objectives_       = numberOfObjectives;        
	    dominance_        = new DominanceComparator();
	    equals_           = new EqualSolutions();
	    distance_         = new Distance();
	    idealComparator = new IdealComparator();
		zp_  = new double[objectives_];
		nzp_  = new double[objectives_];
    	initIdealPoint();
	}

	private void initNadirPoint() {
		for (int i = 0; i < objectives_; i++)
		  nzp_[i] = -1.0e+30;

		for (int i = 0; i < size(); i++)
		  updateNadirPoint(this.get(i), nzp_);
	} 

	private boolean updateNadirPoint(Solution indiv, double[] nz_) {
		boolean changed = false, flag = false;
		for (int i = 0; i < objectives_; i++) {
		  flag = (indiv.getObjective(i) > nz_[i]);
		  changed = (changed || flag);
		  if (flag)
		    nz_[i] = indiv.getObjective(i);
		}
		return changed;
	}

	private void initIdealPoint(){
		for (int i = 0; i < objectives_; i++)
	      	zp_[i] = 1.0e+30;

	    for (int i = 0; i < size(); i++)
	      updateReference(this.get(i), zp_);
	}

	private boolean updateReference(Solution indiv, double[] z_) {
	    boolean changed = false, flag = false;
	    for (int i = 0; i < objectives_; i++) {
	      flag = (indiv.getObjective(i) < z_[i]);
	      changed = (changed || flag);
	      if (flag) {
	        z_[i] = indiv.getObjective(i);
	      }
	    }
	    return changed;
	}

	public void addSet(SolutionSet offspring, double[] zp_, double[] nzp_) {
		
		this.zp_ = zp_;
		this.nzp_ = nzp_;

		updateDistanceToIdealPoint();
	    
	    for (int i = 0; i< offspring.size(); ++i) {
	    	updateArchive(offspring.get(i));
	    }
	}

	public boolean add(Solution solution) {
	    updateReference(solution, zp_);
	    updateNadirPoint(solution, nzp_);
	    updateDistanceToIdealPoint();
	    return updateArchive(solution);
	}

	public boolean updateArchive (Solution solution){

	    int flag = 0;
	    int i = 0;
	    Solution aux; //Store an solution temporally
	    while (i < solutionsList_.size()){
	      aux = solutionsList_.get(i); 
	            
	      flag = dominance_.compare(solution,aux);
	      if (flag == 1) {               // The solution to add is dominated
	        return false;                // Discard the new solution
	      } else if (flag == -1) {       // A solution in the archive is dominated
	      	System.out.println("removed: "+solutionsList_.get(i));
	        solutionsList_.remove(i);    // Remove it from the population            
	      } else {
	          if (equals_.compare(aux,solution)==0) { // There is an equal solution 
	        	                                      // in the population
	            return false; // Discard the new solution
	          }  // if
	          i++;
	      } 
	    }

	    solution.setDistanceToIdealPoint(getDistance(solution));

	    System.out.println("inserted: "+solution); 
	    solutionsList_.add(solution);

	    if (size() > maxSize_){			
		    int worst = indexWorst(idealComparator);
		    System.out.println("removed: "+get(worst));
		    remove(worst);
		}
	    return true;

	}

	private double getDistance(Solution s){

		Solution idealSolution = new Solution(objectives_);
		Solution aux           = new Solution(objectives_);

		for (int i = 0; i<objectives_; ++i) {
			idealSolution.setObjective(i, 0.0);
			aux.setObjective(i, (s.getObjective(i) - zp_[i]) / (((nzp_[i] - zp_[i]) == 0) ? (double)1 : (double)(nzp_[i] - zp_[i])));
		}
		return distance_.distanceBetweenObjectives(aux, idealSolution);
	}

	public String toString(){
	   return "Archiver: Ideal";
	}

	public void updateDistanceToIdealPoint(){
		Solution aux;
		for (int i=0; i< size(); i++) {
    		aux = get(i);
    		aux.setDistanceToIdealPoint(getDistance(aux));
    	}
	}

}