//DominanceAndDecompositionArchiveTest.java

package jmetal.util.archive;

import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.problems.WFG.WFG1;
import jmetal.qualityIndicator.R2;

public class DominanceAndDecompositionArchiveTest {

	public static void main(String[] args) {
		DominanceAndDecompositionArchive archive = new DominanceAndDecompositionArchive(3, 2);
  		R2 r2;

		try {

			Problem problem = new WFG1("Real", 2, 4, 2); // 2 objectives

			r2 = new R2(problem.getNumberOfObjectives(), ("weightVectors/"+problem.getNumberOfObjectives()+"m.dat"));

			Solution s1 = new Solution(problem);
			s1.setObjective(0,0.25);
			s1.setObjective(1,0.25);
			//archive.add(s1);

			Solution s2 = new Solution(problem);
			s2.setObjective(0,0.0);
			s2.setObjective(1,1.0);
			//archive.add(s2);

			Solution s3 = new Solution(problem);
			s3.setObjective(0,0.5);
			s3.setObjective(1,0.5);
			//archive.add(s3);



	/*		for (int i=0; i<archive.size() ;++i ) {
				Solution aux = archive.get(i);
				System.out.println(aux);
				System.out.println(aux.readRegion());
			}
	*/
			//archive.printObjectives();

			Solution s4 = new Solution(problem);
			s4.setObjective(0,1.0);
			s4.setObjective(1,0.0);
			//archive.add(s4);
		
			SolutionSet aux = new SolutionSet(4);
			aux.add(s1);
			//aux.add(s2);
			aux.add(s3);
			// aux.add(s4);
			
			archive.addSet(aux);
			double value = r2.not_normalized_R2(archive);
			System.out.println(value);

			// System.out.println(s1+" : "+s1.readRegion());
			// System.out.println(s2+" : "+s2.readRegion());
			// System.out.println(s3+" : "+s3.readRegion());
			// System.out.println(s4+" : "+s4.readRegion());

			System.out.println();
			archive.printObjectives();

			SolutionSet a = new SolutionSet(4);
			a.add(s4);
			a.add(s2);
			archive.addSet(a);

		 	value = r2.not_normalized_R2(archive);
			System.out.println(value);

			System.out.println();
			archive.printObjectives();
			

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

}