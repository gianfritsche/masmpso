//  Archive.java
//
//  Author:
//       Antonio J. Nebro <antonio@lcc.uma.es>
//       Juan J. Durillo <durillo@lcc.uma.es>
//       Gian M. Fritsche <gmfritsche@inf.ufpr.br>
//
//  Copyright (c) 2011 Antonio J. Nebro, Juan J. Durillo
//  Copyright (c) 2015 Gian M. Fritsche
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package jmetal.util.archive;

import jmetal.core.SolutionSet;
import jmetal.core.Solution;

/**
 * This class represents the super class for archive objects.
 */
public abstract class Archive extends SolutionSet {

  public Archive (int size) {
    super(size);
  }

  public void addSet(SolutionSet offspring, double[] zp_, double[] nzp_) { 
  	for (int i=0; i<offspring.size(); i++) {
  		add(offspring.get(i));
  	}
  }

  /** 
  * Do not share solution list
  * instead of share, add the olds solutions to new archivers when change
  */
  // // observer method for update attributes when shared solutionList_
  // public void archiveChanged() { 
  // 	// default: do nothing
  // } 

} // Archive
