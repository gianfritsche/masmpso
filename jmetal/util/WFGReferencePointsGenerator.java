// WFGReferencePointsGenerator.java
//
//  Author:
//       Gian M. Fritsche <gmfritsche@inf.ufpr.br>
//
//  Copyright (c) 2015 Gian M. Fritsche
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package jmetal.util;

import jmetal.core.SolutionSet;
import jmetal.core.Solution;
import jmetal.util.comparators.DominanceComparator;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class WFGReferencePointsGenerator {

	public int[] objectives = {2, 3, 5, 8, 10, 15, 20};

	public String inputDirectory = "weightVectors";
	public String outputDirectory = "referencePoints";
	public String paretoDirectory = "pareto";

	private SolutionSet nonDominatedSet;
	private DominanceComparator comparator;

	public WFGReferencePointsGenerator() {

		for (int m : objectives) {
			double [][] lambda = loadWeightVectors(m);
			generateReferencePoints(lambda, m);
		}
	}

	public void writeToFile(double [][] references, String file){
		System.out.println(file);
		try { 
			PrintWriter out = new PrintWriter(file);
			for ( double [] reference : references) {
				for ( double d : reference) {
					out.print(d+"\t");
				}
				out.println();
			}
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	/**
	* @author Giovanna Garcia 14/05/2015 
	**/
	public void generateReferencePoints(double [][] lambda, int m){
		
		double[] nz = new double[m];
		double[] z = new double[m];
		int d=3;
		for (int i=0; i!=m; ++i, d+=2) {
			z[i]=0;
			nz[i]=d;
		}

		for (int i=1; i<=9; i++){ // for each WFG problem

			nonDominatedSet = new SolutionSet();
			nonDominatedSet.setCapacity(Integer.MAX_VALUE);
			comparator = new DominanceComparator();

			double[][] references = new double[lambda.length][m];
			double[] distances = new double[lambda.length]; 
			for (int b=0; b!= lambda.length; ++b)
				distances[b] = Double.MAX_VALUE;
			try {
				String file = paretoDirectory+"/WFG"+i+"_"+m+".pf";
				FileInputStream fis = new FileInputStream(file);
				InputStreamReader isr = new InputStreamReader(fis);
				BufferedReader br = new BufferedReader(isr);
				String aux = br.readLine();
				aux = br.readLine();
				int line = 1;
				while (aux != null) {
					System.out.println("WFG"+i+"_"+m+": "+(line++));

					StringTokenizer st = new StringTokenizer(aux);
					int j = 0;
					double [] solution = new double[m];
					Solution newsolution = new Solution(m);
					while (st.hasMoreTokens()) {
						double value = (new Double(st.nextToken())).doubleValue();
						solution[j] = value;
						newsolution.setObjective(j, value);
						j++;
					}

					int dominance = 0;
					for (int k = 0; k < nonDominatedSet.size() && dominance != 1 ; ++k ) {
						dominance = comparator.compare(newsolution, nonDominatedSet.get(k));
						if (dominance == -1) {
							nonDominatedSet.remove(k--);
						}
					}

					if (dominance != 1)
						nonDominatedSet.add(newsolution);

					aux = br.readLine();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			for (int j = 0; j<nonDominatedSet.size(); ++j) {
				double perc = (double) j / (double) nonDominatedSet.size();
				perc *= 100;
				System.out.println("Write to File ["+perc+"%]");
				double [] solution = new double[m];
				Solution sol = nonDominatedSet.get(j);
				for (int k = 0; k<m; ++k) {
					solution[k] = sol.getObjective(k);
				}
				for (int a=0; a != lambda.length; ++a) {
					double dist = calculateDistance2(solution, lambda[a], z, nz, m);
					if (dist < distances[a]) {
						distances[a] = dist;
						references[a] = solution;
					}
				}
			}

			writeToFile(references, outputDirectory+"/WFG"+i+"_"+m+".ref");
		}
	}

	public double calculateDistance2(double[] solution, double[] lambda,
	      double[] z_, double[] nz_, int numberOfObjectives) {

	    // normalize the weight vector (line segment)
	    double nd = norm_vector(lambda, numberOfObjectives);
	    for (int i = 0; i < numberOfObjectives; i++)
	      lambda[i] = lambda[i] / nd;

	    double[] realA = new double[numberOfObjectives];
	    double[] realB = new double[numberOfObjectives];

	    // difference between current point and reference point
	    for (int i = 0; i < numberOfObjectives; i++)
	      realA[i] = (solution[i] - z_[i]);

	    // distance along the line segment
	    double d1 = Math.abs(innerproduct(realA, lambda));

	    // distance to the line segment
	    for (int i = 0; i < numberOfObjectives; i++)
	      realB[i] = (solution[i] - (z_[i] + d1 * lambda[i]));

	    double distance = norm_vector(realB, numberOfObjectives);

	    return distance;
	}

	public double innerproduct(double[] vec1, double[] vec2) {
		double sum = 0;

		for (int i = 0; i < vec1.length; i++)
		  sum += vec1[i] * vec2[i];

		return sum;
	}

	public double norm_vector(double[] z, int numberOfObjectives) {
		double sum = 0;

		for (int i = 0; i < numberOfObjectives; i++)
		  sum += z[i] * z[i];

		return Math.sqrt(sum);
	}

	public double[][] loadWeightVectors(int m) {
		double[][] lambda = null;
		try {
			FileInputStream fis = new FileInputStream(inputDirectory + "/" + m + ".dat");
			InputStreamReader isr = new InputStreamReader(fis);
			BufferedReader br = new BufferedReader(isr);

			int j, i=0;
			String aux = br.readLine();
			StringTokenizer tokenizer = new StringTokenizer(aux);
			int w = (new Integer(tokenizer.nextToken())).intValue();
		 	lambda = new double[w][m];
			aux = br.readLine();
			while (aux != null) {
			  StringTokenizer st = new StringTokenizer(aux);
			  j = 0;
			  while (st.hasMoreTokens()) {
			    double value = (new Double(st.nextToken())).doubleValue();
			    lambda[i][j++] = value;
			  }
			  i++;
			  aux = br.readLine();
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lambda;
	}

	public static void main(String[] args) {
		new WFGReferencePointsGenerator();
	}

}
