// DTLZReferencePointsGenerator.java
//
//  Author:
//       Gian M. Fritsche <gmfritsche@inf.ufpr.br>
//
//  Copyright (c) 2015 Gian M. Fritsche
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package jmetal.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class DTLZReferencePointsGenerator {

	public int[] objectives = {2, 3, 5, 8, 10, 15, 20};

	public String inputDirectory = "weightVectors";
	public String outputDirectory = "IGDreferencePoints";

	public DTLZReferencePointsGenerator() {
	
		for (int m : objectives) {
			try {
				FileInputStream fis = new FileInputStream(inputDirectory + "/" + m + ".dat");
				InputStreamReader isr = new InputStreamReader(fis);
				BufferedReader br = new BufferedReader(isr);

				int j, i=0;
				String aux = br.readLine();
				StringTokenizer tokenizer = new StringTokenizer(aux);
				int w = (new Integer(tokenizer.nextToken())).intValue();
				double [][] lambda = new double[w][m];
				double [] sum = new double[w];
				aux = br.readLine();
				while (aux != null) {
				  StringTokenizer st = new StringTokenizer(aux);
				  j = 0;
				  sum [i] = .0;
				  while (st.hasMoreTokens()) {
				    double value = (new Double(st.nextToken())).doubleValue();
				    lambda[i][j++] = value;
				    sum[i] += (value * value);
				  }
				  i++;
				  aux = br.readLine();
				}
				GenerateDTLZ1ReferencePoints(lambda, m);
				GenerateDTLZ234ReferencePoints(lambda, sum, m);  
				br.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void GenerateDTLZ1ReferencePoints(double [][] lambda, int m) {
		try { 
			PrintWriter out = new PrintWriter(outputDirectory+"/DTLZ1_"+m+".ref");
			for ( double [] w : lambda) {
				for ( double d : w) {
					out.print((0.5*d)+"\t");
				}
				out.println();
			}
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void GenerateDTLZ234ReferencePoints(double [][] lambda , double [] sum, int m) {
		try { 
			PrintWriter out = new PrintWriter(outputDirectory+"/DTLZ2-4_"+m+".ref");
			int i = 0;
			for ( double [] w : lambda) {
				for ( double d : w) {
					out.print((d/Math.sqrt(sum[i]))+"\t");
				}
				i++;
				out.println();
			}
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		new DTLZReferencePointsGenerator();
	}

}