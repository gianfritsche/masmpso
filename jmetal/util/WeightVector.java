//  WeightVector.java
//
//  Author:
//       Gian M. Fritsche <gmfritsche@inf.ufpr.br>
//
//  Copyright (c) 2015 Gian M. Fritsche
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package jmetal.util;

import java.util.Vector;
import jmetal.core.Solution;
/**
 * This class defines a WeightVector used on DominanceAndDecompositionArchive
 */
public class WeightVector {

  /**
  * The weigth vector
  */
  private Vector<Double> weigths;

  /**
  * The solution set of the solutions associated with this weight vector
  */
  private Vector<Solution> associated;


  /**
  * Constructor.
  * Creates an empty instance of WeightVector of size n
  */
  public WeightVector(int n) {
    weigths = new Vector<Double>(n);
    associated = new Vector<Solution>();
  } // WeightVector

  /**
  * Constructor.
  * Creates an instance of WeightVector
  * based in a weight vector input
  */
  public WeightVector(Vector<Double> input) {
    weigths = input;
    associated = new Vector<Solution>();
  } // WeightVector


  /**
  * add a new dimension to the weight vector
  */
  public void add (double w){
    weigths.add(w);
  }

  public String toString(){
    String aux = "";
    for (Double d : weigths) {
      aux += "\t"+d;
    }
    return aux;
  }

  public boolean addAssociated (Solution s) {
    return associated.add(s);
  }

  public boolean removeAssociated(Solution s) {
    return associated.remove(s);
  }

  public double checkAngle(Solution s) {
    double dot, lenx, lenw, x, w;
    dot = lenw = lenx = 0.0;
    for (int i = 0; i<weigths.size(); ++i) {
      x = s.getObjective(i);
      w = weigths.get(i);
      dot += (x*w);
      lenx += (x*x);
      lenw += (w*w);
    }
    return Math.acos(dot/(Math.sqrt(lenx * lenw)));
  }

} // WeightVector

