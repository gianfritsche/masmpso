//  IdealComparator.java
//
//  Author:
//       Gian Fritsche <gmfritsche@inf.ufpr.br>
//
//  Copyright (c) 2015 Gian M. Fritsche
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package jmetal.util.comparators;

import jmetal.core.Solution;

import java.util.Comparator;


public class IdealComparator implements Comparator{    

  /** 
   * stores a comparator for check the rank of solutions
   */
  private static final Comparator comparator = new RankComparator();
  
  public int compare(Object o1, Object o2) {
    if (o1==null)
      return 1;
    else if (o2 == null)
      return -1;
    
    int flagComparatorRank = comparator.compare(o1,o2);
    if (flagComparatorRank != 0)
      return flagComparatorRank;
    
    double distance1 = ((Solution)o1).getDistanceToIdealPoint();
    double distance2 = ((Solution)o2).getDistanceToIdealPoint();
    if (distance1 <  distance2)
      return -1;
    
    if (distance1 > distance2)
      return 1;
      
    return 0;    
  } // compare
} // IdealComparator
