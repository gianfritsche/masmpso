//  EfficientNonDominationLevelUpdateFramework.java
//
//  Author:
//       Gian M. Fritsche <gmfritsche@inf.ufpr.br>
//
//  Copyright (c) 2015 Gian M. Fritsche
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package jmetal.util;

import java.util.Vector;
import java.util.List;
import java.util.ArrayList;
import jmetal.core.Solution;
import jmetal.util.comparators.DominanceComparator;
/**
 * This class defines a EfficientNonDominationLevelUpdateFramework
 * used on DominanceAndDecompositionArchive
 */
public class EfficientNonDominationLevelUpdateFramework {

  /**
  * The non-domination structure represented by a matrix
  * each row represent a level
  */
  private List< Vector<Solution> > structure;

  private DominanceComparator comparator;

  /**
  * Constructor.
  * Creates an empty instance of EfficientNonDominationLevelUpdateFramework
  */
  public EfficientNonDominationLevelUpdateFramework() {
    structure  = new ArrayList<Vector<Solution>>();
    comparator = new DominanceComparator();
  }

  /**
  * add a new solution and
  * update the structure
  * @param the offspring solution xc
  */
  public void afterOffspringGeneration(Solution xc){
    boolean flag1, flag2, flag3;
    flag3 = flag2 = flag1 = false;
    int i;
    for (i=0; i<structure.size(); ++i) {
      Vector<Solution> S = new Vector<Solution>();
      Vector<Solution> F = structure.get(i);
      for (Solution j : F) {
        int aux = comparator.compare(xc, j);
        if (aux == 0) {
          flag1 = true;
        } else if (aux == 1) {
          flag2 = true;
          break;
        } else {
          flag3 = true;
          S.add(j);
        }
      }
      for(Solution s : S) {
        F.remove(s);
      }
      if(flag2){
        break;
      } else if (!flag1 && flag3) {
        structure.add(i, new Vector<Solution>());
        break;
      } else {
        update(S, i+1);
        break;
      }
    }
    // if xc does not belong to any existing non-domination level
    if(i==structure.size()){ 
      // add into a new created non-domination level
      Vector<Solution> l = new Vector<Solution>();
      l.add(xc);
      structure.add(l);
    } else {
      structure.get(i).add(xc);
    }

  }

  /**
  * update the structure
  * @param non-domination level index i
  */
  public void update(Vector<Solution> S, int i) {
    if (!(i < structure.size())) {
      structure.add(S);
    } else {
      Vector<Solution> F = structure.get(i);
      for (Solution s : S) {
        F.add(s);
      }
      Vector<Solution> T = new Vector<Solution>();
      for (Solution s : S) {
        for (Solution f : F ) {
          if (comparator.compare(s, f) == -1){
            T.add(f);
          }
        }
        for(Solution t: T) {
          F.remove(t);
        }
      }
      if(T.size()==0){
        update(T, i+1);
      }
    }
  }

  /**
  *
  */
  public void afterUpdateProcedure(Solution xe){

  }

  public String toString() {
    String aux = "";
    for (Vector<Solution> set : structure) {
      for (Solution s : set ) {
        /**
        * @TODO
        */
      }
    }
    return aux;
  }


} // EfficientNonDominationLevelUpdateFramework

