//SigmaLeaderSelection.java

 /**
 * 
 * Author:
 *    Gian M. Fritsche <gmfritsche@inf.ufpr.br>
 * 
 * Copyright (c) 2015 Gian M. Fritsche
 * 
 * Note: This is a free software developed based on the open source project 
 * jMetal<http://jmetal.sourceforge.net>. The copy right of jMetal belongs to 
 * its original authors, Antonio J. Nebro and Juan J. Durillo. Nevertheless, 
 * this current version can be redistributed and/or modified under the terms of 
 * the GNU Lesser General Public License as published by the Free Software 
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package jmetal.metaheuristics.smpso.leaderSelection;

import java.util.ArrayList;
import jmetal.metaheuristics.smpso.GenericSMPSO;
import java.util.List;
import jmetal.core.Solution;
import jmetal.util.Distance;
import java.math.*;

public class SigmaLeaderSelection implements LeaderSelection {

 	private List< double[] > sigmaVectors;
 	private int objectives_;
	private Distance distance;

	private double[] zp_;   // ideal point for Pareto-based population
	private double[] nzp_;  // nadir point for Pareto-based population

 	public SigmaLeaderSelection(int objectives_) {
 		this.objectives_ = objectives_;
 		distance = new Distance();
    }

	public double[] calculateSigmaVector(Solution s) {
		int cont = 0;
		List<Double> sigma = new ArrayList<Double>();
		for (int i = 0; i < objectives_-1; ++i ) {
			for (int j = i+1; j < objectives_; ++j ) {
				double obj1 = (s.getObjective(i) - zp_[i]) / (((nzp_[i] - zp_[i]) == 0) ? (double)1 : (double)(nzp_[i] - zp_[i]));
				double obj2 = (s.getObjective(j) - zp_[j]) / (((nzp_[j] - zp_[j]) == 0) ? (double)1 : (double)(nzp_[j] - zp_[j]));
				sigma.add(calculateSigma(obj1, obj2));
			}		
		}
		double[] vector = new double[sigma.size()];
		for (double d : sigma) {
			vector[cont++] = d;
		}
		return vector;
	}

	private double calculateSigma(double f1, double f2){
		double value = (f1*f1) - (f2*f2);
		double denominator = (f1*f1)+ (f2*f2);
		if(denominator!=0)
			return  value/denominator;
		return 0;
	}

	public Solution getLeader(Solution particle, List<Solution> solutionsList_, double[] zp_, double[] nzp_) {
		this.zp_ = zp_;
		this.nzp_ = nzp_;

		sigmaVectors = new ArrayList< double[] >();
		for (Solution s : solutionsList_ ) {
			sigmaVectors.add(calculateSigmaVector(s));
		}

		double[] sigma = calculateSigmaVector(particle); 
 		Solution leader = solutionsList_.get(0);
 		double value = computeDistance(sigma, sigmaVectors.get(0));

 		for (int i = 1; i<solutionsList_.size(); ++i) {
 			
 			double distance = computeDistance(sigma, sigmaVectors.get(i));	

 			if (distance < value) {
 				value = distance;
 				leader = solutionsList_.get(i);
 			}
 		}

 		return leader;				
 	}

 	private double computeDistance (double[] p, double[] l) {
 		Solution ps = new Solution(p.length);
 		Solution ls = new Solution(l.length);
 		for (int i=0; i<p.length; ++i) {
 			ps.setObjective(i, p[i]);
 			ls.setObjective(i, l[i]);
 		}
 		return distance.distanceBetweenObjectives(ps, ls);
 	}


	public String toString(){
		return "Leader Selection: Sigma";
	}

 }