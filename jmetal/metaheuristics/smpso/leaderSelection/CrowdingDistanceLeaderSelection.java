// CrowdingDistanceLeaderSelection.java

 /**
 * 
 * Author:
 *    Gian M. Fritsche <gmfritsche@inf.ufpr.br>
 * 
 * Copyright (c) 2015 Gian M. Fritsche
 * 
 * Note: This is a free software developed based on the open source project 
 * jMetal<http://jmetal.sourceforge.net>. The copy right of jMetal belongs to 
 * its original authors, Antonio J. Nebro and Juan J. Durillo. Nevertheless, 
 * this current version can be redistributed and/or modified under the terms of 
 * the GNU Lesser General Public License as published by the Free Software 
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package jmetal.metaheuristics.smpso.leaderSelection;

import java.util.List;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.util.comparators.CrowdingDistanceComparator;
import jmetal.util.Distance;
import jmetal.util.PseudoRandom;

public class CrowdingDistanceLeaderSelection implements LeaderSelection {

 	private int objectives_;
 	private Distance distance_;
 	private CrowdingDistanceComparator comparator;

	public CrowdingDistanceLeaderSelection(int objectives_) {
		this.objectives_ = objectives_;
		this.distance_ = new Distance();
		comparator = new CrowdingDistanceComparator();
	}

	public Solution getLeader(Solution particle, List<Solution> solutionsList_, double[] zp_, double[] nzp_) {
		
		SolutionSet set = new SolutionSet();
		int i = 0;
		for (Solution s : solutionsList_)
			set.add(i++, s);
		distance_.crowdingDistanceAssignment(set, objectives_);

 		Solution one, two;
		int pos1 = PseudoRandom.randInt(0, solutionsList_.size() - 1);
		int pos2 = PseudoRandom.randInt(0, solutionsList_.size() - 1);
		one = solutionsList_.get(pos1);
		two = solutionsList_.get(pos2);

		if (comparator.compare(one, two) < 1)
			return one;
		return two;
 	}


	public String toString(){
		return "Leader Selection: Crowding Distance";
	}

}
