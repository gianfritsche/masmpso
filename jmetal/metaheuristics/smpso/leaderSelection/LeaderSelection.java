// LeaderSelection.java

 /**
 * 
 * Author:
 *    Gian M. Fritsche <gmfritsche@inf.ufpr.br>
 * 
 * Copyright (c) 2015 Gian M. Fritsche
 * 
 * Note: This is a free software developed based on the open source project 
 * jMetal<http://jmetal.sourceforge.net>. The copy right of jMetal belongs to 
 * its original authors, Antonio J. Nebro and Juan J. Durillo. Nevertheless, 
 * this current version can be redistributed and/or modified under the terms of 
 * the GNU Lesser General Public License as published by the Free Software 
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package jmetal.metaheuristics.smpso.leaderSelection;

import jmetal.core.Solution;
import java.util.List;

public interface LeaderSelection {

	/**
	* @method getLeader
	* @return <Solution> leader
	* @param:
	* 	<Solution>       reference particle
	* 	<List<Solution>> solutionList_ to get leader from
	* 	<double[]>       zp_:  ideal point (used by most leader selection methods)
	* 	<double[]>       nzp_: nadir point (used by most leader selection methods)
	*/
	public Solution getLeader(Solution particle, List<Solution> solutionsList_, double[] zp_, double[] nzp_); 

}