//NWSumLeaderSelection.java

 /**
 * 
 * Author:
 *    Gian M. Fritsche <gmfritsche@inf.ufpr.br>
 * 
 * Copyright (c) 2015 Gian M. Fritsche
 * 
 * Note: This is a free software developed based on the open source project 
 * jMetal<http://jmetal.sourceforge.net>. The copy right of jMetal belongs to 
 * its original authors, Antonio J. Nebro and Juan J. Durillo. Nevertheless, 
 * this current version can be redistributed and/or modified under the terms of 
 * the GNU Lesser General Public License as published by the Free Software 
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package jmetal.metaheuristics.smpso.leaderSelection;

import java.util.List;
import jmetal.core.Solution;

public class NWSumLeaderSelection implements LeaderSelection {

	private int objectives_;
	private double[] zp_;   // ideal point for Pareto-based population
	private double[] nzp_;  // nadir point for Pareto-based population

 	public NWSumLeaderSelection(int objectives_) {
 		this.objectives_ = objectives_;
	}

	public Solution getLeader(Solution particle, List<Solution> solutionsList_, double[] zp_, double[] nzp_) {
		this.zp_ = zp_;
		this.nzp_ = nzp_;
		
		Solution leader = solutionsList_.get(0);
 		double value = computeNWSum(particle, leader);
 		for (Solution s : solutionsList_ ) {
 			double nwsum = computeNWSum(particle, s);
 			if (nwsum > value) {
 				value = nwsum;
 				leader = s;
 			}
 		}
 		return leader;
 	}

 	private double computeNWSum(Solution particle, Solution leader) {
 		double res, parc, nwsum;
 		res = parc = nwsum = 0;
 		for (int i = 0; i < particle.getNumberOfObjectives(); ++i) {
 			res=0;
 			for (int j = 0; j < particle.getNumberOfObjectives(); ++j) {
 				res += (particle.getObjective(j) - zp_[j]) / (((nzp_[j] - zp_[j]) == 0) ? (double)1 : (double)(nzp_[j] - zp_[j]));
 			}

 			if (res == 0.0) {
 				res = 1.0;
 			}
 			parc = ((particle.getObjective(i) - zp_[i]) / (((nzp_[i] - zp_[i]) == 0) ? (double)1 : (double)(nzp_[i] - zp_[i]))) / res;
	 		nwsum += parc * (leader.getObjective(i) - zp_[i]) / (((nzp_[i] - zp_[i]) == 0) ? (double)1 : (double)(nzp_[i] - zp_[i]));
 		}
 		return nwsum;
 	}


	public String toString(){
		return "Leader Selection: NWSum";
	}

 }