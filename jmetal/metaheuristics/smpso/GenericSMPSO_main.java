//  GenericSMPSO_main.java
//
//  Author:
//       Antonio J. Nebro <antonio@lcc.uma.es>
//       Gian M. Fritsche <gmfritsche@inf.ufpr.br>
//
//  Copyright (c) 2013 Antonio J. Nebro
//  Copyright (c) 2015 Gian M. Fritsche
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package jmetal.metaheuristics.smpso;

import jmetal.core.Algorithm;
import jmetal.core.Problem;
import jmetal.core.SolutionSet;
import jmetal.core.Solution;
import jmetal.metaheuristics.smpso.leaderSelection.LeaderSelectionFactory;
import jmetal.metaheuristics.smpso.leaderSelection.LeaderSelection;
import jmetal.operators.mutation.Mutation;
import jmetal.operators.mutation.MutationFactory;
import jmetal.problems.Kursawe;
import jmetal.problems.ProblemFactory;
import jmetal.qualityIndicator.QualityIndicator;
import jmetal.util.archive.Archive;
import jmetal.util.archive.ArchiverFactory;
import jmetal.util.Configuration;
import jmetal.util.JMException;
import jmetal.util.parallel.IParallelEvaluator;
import jmetal.util.parallel.MultithreadedEvaluator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import jmetal.problems.WFG.WFG1;

/**
 * This class executes GenericSMPSO, a multithreaded version of SMPSO, characterized
 * by evaluating the particles in parallel.
 */
public class GenericSMPSO_main {
  public static Logger      logger_ ;      // Logger object
  public static FileHandler fileHandler_ ; // FileHandler object

  /**
   * @param args Command line arguments. The first (optional) argument specifies 
   *             the problem to solve.
   * @throws JMException 
   * @throws IOException 
   * @throws SecurityException 
   * Usage: three options
   *      - jmetal.metaheuristics.smpso.GenericSMPSO_main
   *      - jmetal.metaheuristics.smpso.GenericSMPSO_main problemName
   *      - jmetal.metaheuristics.smpso.GenericSMPSO_main problemName ParetoFrontFile
   */
  public static void main(String [] args) throws JMException, IOException, ClassNotFoundException {
    Problem   problem   ;  // The problem to solve
    Algorithm algorithm ;  // The algorithm to use
    Mutation  mutation  ;  // "Turbulence" operator

    QualityIndicator indicators ; // Object to get quality indicators

    HashMap  parameters ; // Operator parameters

    // Logger object and file to store log messages
    logger_      = Configuration.logger_ ;
    fileHandler_ = new FileHandler("SMPSO_main.log"); 
    logger_.addHandler(fileHandler_) ;

    indicators = null ;
    if (args.length == 1) {
      Object [] params = {"Real"};
      problem = (new ProblemFactory()).getProblem(args[0],params);
    } // if
    else if (args.length == 2) {
      Object [] params = {"Real"};
      problem = (new ProblemFactory()).getProblem(args[0],params);
      indicators = new QualityIndicator(problem, args[1]) ;
    } // if
    else { // Default problem
      //problem = new Kursawe("Real", 3); 
      //problem = new Water("Real");
      //problem = new ZDT1("ArrayReal", 1000);
      //problem = new ZDT4("BinaryReal");
      //problem = new WFG1("Real");
      //problem = new DTLZ1("Real");
      //problem = new OKA2("Real") ;
      //problem = new WFG1("Real", 4, 12, 5); // 5 objectives
      problem = new WFG1("Real", 2, 4, 2); // 2 objectives
    } // else

    int threads = 0 ; // 0 - use all the available cores
    //int threads = 4 ; 
    IParallelEvaluator parallelEvaluator = new MultithreadedEvaluator(threads) ;

    int archiveSize = 100;

    Archive archive = (new ArchiverFactory()).getArchiver(ArchiverFactory.MGA_ARCHIVE, archiveSize, problem.getNumberOfObjectives());
    //Archive archive = (new ArchiverFactory()).getArchiver(ArchiverFactory.DOMINANCE_AND_DECOMPOSITION_ARCHIVE, archiveSize, problem.getNumberOfObjectives());

    // LeaderSelection leaderSelection = (new LeaderSelectionFactory()).getLeaderSelection(LeaderSelectionFactory.CROWDING_LEADER_SELECTION, solutionList, problem.getNumberOfObjectives());
    LeaderSelection leaderSelection = (new LeaderSelectionFactory()).getLeaderSelection(LeaderSelectionFactory.NWSUM_LEADER_SELECTION, problem.getNumberOfObjectives());
    // LeaderSelection leaderSelection = (new LeaderSelectionFactory()).getLeaderSelection(LeaderSelectionFactory.SIGMA_LEADER_SELECTION, solutionList, problem.getNumberOfObjectives());

    algorithm = new GenericSMPSO(problem, parallelEvaluator, archive, leaderSelection) ;

    // Algorithm parameters    
    algorithm.setInputParameter("swarmSize",100);
    algorithm.setInputParameter("archiveSize",archiveSize);
    algorithm.setInputParameter("maxIterations",1000);

    parameters = new HashMap() ;
    parameters.put("probability", 1.0/problem.getNumberOfVariables()) ;
    parameters.put("distributionIndex", 20.0) ;
    mutation = MutationFactory.getMutationOperator("PolynomialMutation", parameters);                    

    algorithm.addOperator("mutation", mutation);

    // Execute the Algorithm 
    long initTime = System.currentTimeMillis();
    SolutionSet population = algorithm.execute();
    long estimatedTime = System.currentTimeMillis() - initTime;

    // Result messages 
    logger_.info("Total execution time: "+estimatedTime + "ms");
    logger_.info("Objectives values have been writen to file FUN");
    population.printObjectivesToFile("FUN");
    logger_.info("Variables values have been writen to file VAR");
    population.printVariablesToFile("VAR");      

    if (indicators != null) {
      logger_.info("Quality indicators") ;
      logger_.info("Hypervolume: " + indicators.getHypervolume(population)) ;
      logger_.info("GD         : " + indicators.getGD(population)) ;
      logger_.info("IGD        : " + indicators.getIGD(population)) ;
      logger_.info("Spread     : " + indicators.getSpread(population)) ;
      logger_.info("Epsilon    : " + indicators.getEpsilon(population)) ;
    } // if                   
  } //main
} // SMPSO_main
