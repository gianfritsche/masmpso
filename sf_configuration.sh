#!/bin/bash

## sf_configuration.sh
# Configure Scale Factor (SF) parameter from Adaptive Choice Function (ACF)
# using UFPR servers without exceed 2GB quota

dependencies=".:lib/commons-math3-3.5/commons-math3-3.5.jar"
javac_options="-J-XX:MaxHeapSize=256m -J-Xmx512m"
java_options="-XX:MaxHeapSize=256m -Xmx512m"

function log () {
	echo -e "\e[1;34m $1 \e[0m"
}

function checkfordependencies () {
	echo "checking for dependencies"
	if [ ! -d "lib" ];
	then
		echo "making lib directory"
		mkdir lib
	fi
	if [ ! -f "lib/commons-math3-3.5/commons-math3-3.5.jar" ];
	then
		echo "downloading commons match dependency"
		cd lib
		wget "http://mirror.cogentco.com/pub/apache//commons/math/binaries/commons-math3-3.5-bin.tar.gz"
		tar -zxvf "commons-math3-3.5-bin.tar.gz"
		cd ..
	fi
}

function compile () {
	checkfordependencies
	echo "fixing non ASCII chars on jmetal..."
	export JAVA_TOOL_OPTIONS=-Dfile.encoding=UTF8
	echo "loading java files..."
	find jmetal/ org/ -name "*.java" > sources.aux
	echo "compiling..."
	javac $javac_options -classpath $dependencies @sources.aux
} # compile

function run () {

	user=gmfritsche
	server=caporal.c3sl.ufpr.br

	log "loading experiment configuration..."
	. configurationFiles/ScaleFactorConfigurationExperiment.conf

	log "creating jar file..."
	find -name "*.class" > sources.aux
	jar cf jmetal.jar @sources.aux

	log "cleaning experiment folder..."
	ssh -o StrictHostKeyChecking=no -l $user $server "rm -r experiment"
	ssh -o StrictHostKeyChecking=no -l $user $server "mkdir experiment"

	log "sending jar and configuration files..."
	scp -r jmetal.jar referencePoints/ weightVectors/ $user@$server:~/experiment

	IFS=',' read -ra mm                   <<< "$mm"
	IFS=',' read -ra iterations           <<< "$iterations"
	IFS=',' read -ra sizes                <<< "$sizes"
	#IFS=',' read -ra problemList          <<< "$problemList"
	IFS=',' read -ra algorithmsToEvaluate <<< "$algorithmsToEvaluate"
	IFS=',' read -ra indicatorList        <<< "$indicatorList"
	
	for sf in 1.0 0.1 10.0 5.0 0.5 0.05; do
		
		echo mean=true > configurationFiles/AdaptiveChoiceFunction.conf
		echo SF=$sf >> configurationFiles/AdaptiveChoiceFunction.conf

		for ((i=0; i<${#mm[@]}; ++i )) ;do
			#for ((j=0; j<${#problemList[@]}; ++j )) ;do
				#echo "${mm[i]} objectives: ${problemList[j]}"

				echo iterations=${iterations[i]} > configurationFiles/SMPSOStudy.conf
				echo size=${sizes[i]}           >> configurationFiles/SMPSOStudy.conf
				echo m=${mm[i]}                 >> configurationFiles/SMPSOStudy.conf
				echo runs=$runs                 >> configurationFiles/SMPSOStudy.conf
				echo problems=$problemList      >> configurationFiles/SMPSOStudy.conf
				echo run=$algorithmsToRun       >> configurationFiles/SMPSOStudy.conf

				log "cleaning configurationFiles and output folders..."
				ssh -o StrictHostKeyChecking=no -l $user $server "rm -r experiment/SMPSOStudy/"
				ssh -o StrictHostKeyChecking=no -l $user $server "rm -r experiment/configurationFiles/"

				log "sending new configurations..."
				scp -r configurationFiles/ $user@$server:~/experiment

				script="cd experiment && java $java_options -cp jmetal.jar jmetal.experiments.studies.SMPSOStudy r configurationFiles/SMPSOStudy.conf"
				ssh -o StrictHostKeyChecking=no -l $user $server $script

				mkdir -p experiment/$sf
				scp -r $user@$server:~/experiment/SMPSOStudy experiment/$sf

			#done			
		done
	done

	echo mean=true > configurationFiles/AdaptiveChoiceFunction.conf
	echo SF=0.5 >> configurationFiles/AdaptiveChoiceFunction.conf

	#TODO: FOR PARAMETER CONFIGURATIONS EXECUTE INDICATORS LOCALLY

}

function acf_norm () {

	log "loading experiment configuration..."
	. configurationFiles/ACFwithNormalization.conf

	IFS=',' read -ra mm                   <<< "$mm"
	IFS=',' read -ra iterations           <<< "$iterations"
	IFS=',' read -ra sizes                <<< "$sizes"
	#IFS=',' read -ra problemList          <<< "$problemList"
	IFS=',' read -ra algorithmsToEvaluate <<< "$algorithmsToEvaluate"
	IFS=',' read -ra indicatorList        <<< "$indicatorList"
		
	echo mean=true  > configurationFiles/AdaptiveChoiceFunction.conf
	# echo SF=$sf    >> configurationFiles/AdaptiveChoiceFunction.conf
	echo norm=true >> configurationFiles/AdaptiveChoiceFunction.conf

	rm experiment.err
	rm experiment.out

	for ((i=0; i<${#mm[@]}; ++i )) ;do
	
		echo iterations=${iterations[i]} > configurationFiles/SMPSOStudy.conf
		echo size=${sizes[i]}           >> configurationFiles/SMPSOStudy.conf
		echo m=${mm[i]}                 >> configurationFiles/SMPSOStudy.conf
		echo runs=$runs                 >> configurationFiles/SMPSOStudy.conf
		echo problems=$problemList      >> configurationFiles/SMPSOStudy.conf
		echo run=$algorithmsToRun       >> configurationFiles/SMPSOStudy.conf

		# rm -r SMPSOStudy/${mm[i]}/data/ACFFIXED/
		java $java_options -classpath $dependencies jmetal.experiments.studies.SMPSOStudy r configurationFiles/SMPSOStudy.conf 1> experiment.out 2>> experiment.err
		# rsync -auzqP SMPSOStudy/${mm[i]}/data/ACFFIXED/ SMPSOStudy/${mm[i]}/data/ACFNFSFIXED/
		# rm -r SMPSOStudy/${mm[i]}/data/ACFFIXED/

	done
	
	echo mean=true > configurationFiles/AdaptiveChoiceFunction.conf
	echo SF=0.5 >> configurationFiles/AdaptiveChoiceFunction.conf

	#TODO: FOR PARAMETER CONFIGURATIONS EXECUTE INDICATORS LOCALLY

}


function evaluate () {

	log "loading experiment configuration..."
	. configurationFiles/ACFwithNormalization.conf

	IFS=',' read -ra mm                   <<< "$mm"
	IFS=',' read -ra iterations           <<< "$iterations"
	IFS=',' read -ra sizes                <<< "$sizes"
	#IFS=',' read -ra problemList          <<< "$problemList"
	#IFS=',' read -ra algorithmsToEvaluate <<< "$algorithmsToEvaluate"
	IFS=',' read -ra indicatorList        <<< "$indicatorList"
	#IFS=',' read -ra fronts        <<< "$fronts"
	
	rm experiment.err

	for ((i=0; i<${#mm[@]}; ++i )) ;do

		echo ${mm[i]}

		echo iterations=${iterations[i]}     > configurationFiles/SMPSOStudy.conf
		echo size=${sizes[i]}               >> configurationFiles/SMPSOStudy.conf
		echo m=${mm[i]}                     >> configurationFiles/SMPSOStudy.conf
		echo runs=$runs                     >> configurationFiles/SMPSOStudy.conf
		echo problems=$problemList          >> configurationFiles/SMPSOStudy.conf
		echo run=$algorithmsToRun           >> configurationFiles/SMPSOStudy.conf
		echo evaluate=$algorithmsToEvaluate >> configurationFiles/SMPSOStudy.conf
		echo fronts=$fronts                 >> configurationFiles/SMPSOStudy.conf

		java $java_options -classpath $dependencies jmetal.experiments.studies.SMPSOStudy il configurationFiles/SMPSOStudy.conf 1> experiment.out 2>> experiment.err

	done
	
}

# begin main
clear
compile
a=$?
echo "removing aux files..."
rm *.aux

if ! [ $a -eq 0 ]; then
	echo -e "\e[1;31m COMPILATION ERROR! \e[0m"
else 
	echo -e "\e[1;34m COMPILATION SUCCESS! \e[0m"
	if [ "$1" == "eval" ]; then
		evaluate
	elif [ "$1" == "norm" ]; then
		acf_norm
	else 
		run
	fi
fi
echo "done."
# end main
