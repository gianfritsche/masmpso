#!/bin/bash

## multiserver.sh
# Run experiments 
# using multiple UFPR servers without exceed 2GB quota

# server name list
servers=(caporal bowmore cohiba)
user=gmfritsche
# java options
dependencies=".:lib/commons-math3-3.5/commons-math3-3.5.jar"
javac_options="-J-XX:MaxHeapSize=256m -J-Xmx512m"
java_options="-XX:MaxHeapSize=256m -Xmx512m"

function log () {
	echo -e "\e[1;34m $1 \e[0m"
}

function checkfordependencies () {
	echo "checking for dependencies"
	if [ ! -d "lib" ];
	then
		echo "making lib directory"
		mkdir lib
	fi
	if [ ! -f "lib/commons-math3-3.5/commons-math3-3.5.jar" ];
	then
		echo "downloading commons match dependency"
		cd lib
		wget "http://mirror.cogentco.com/pub/apache//commons/math/binaries/commons-math3-3.5-bin.tar.gz"
		tar -zxvf "commons-math3-3.5-bin.tar.gz"
		cd ..
	fi
}

function compile () {
	checkfordependencies
	echo "fixing non ASCII chars on jmetal..."
	export JAVA_TOOL_OPTIONS=-Dfile.encoding=UTF8
	echo "loading java files..."
	find jmetal/ org/ -name "*.java" > sources.aux
	echo "compiling..."
	javac $javac_options -classpath $dependencies @sources.aux
} # compile


function init () {
	# objective index
	i=0
	# problem index
	j=0

	# initializing 'servers in use' flag list
	for (( s = 0; s < ${#servers[@]}; s++ )); do
		S[$s]=0
	done

	. configurationFiles/experiment.conf

	IFS=',' read -ra mm                   <<< "$mm"
	IFS=',' read -ra iterations           <<< "$iterations"
	IFS=',' read -ra sizes                <<< "$sizes"
	IFS=',' read -ra problemList          <<< "$problemList"
	IFS=',' read -ra algorithmsToEvaluate <<< "$algorithmsToEvaluate"
	IFS=',' read -ra indicatorList        <<< "$indicatorList"	
	#IFS=',' read -ra fronts              <<< "$fronts"
			
	echo mean=true   > configurationFiles/AdaptiveChoiceFunction.conf
	echo SF=0.5     >> configurationFiles/AdaptiveChoiceFunction.conf
	echo norm=false >> configurationFiles/AdaptiveChoiceFunction.conf

	log "creating jar file..."
	find -name "*.class" > sources.aux
	jar cf jmetal.jar @sources.aux

	for (( s = 0; s < ${#servers[@]}; s++ )); do
		
		ssh -o StrictHostKeyChecking=no -l $user $server "rm -r ${servers[$s]}/experiment"
		ssh -o StrictHostKeyChecking=no -l $user $server "mkdir ${servers[$s]}/experiment"

		rsync -azquP jmetal.jar referencePoints/ weightVectors/ $user@${servers[$s]}:~/${servers[$s]}/experiment

		mkdir configurationFiles/${servers[$s]}/

	done
}

function next () {
	(( j++ ))
	if [[ $j -ge ${#problemList[@]} ]]; then
		j=0
		(( i++ ))
	fi
}

function run () {
	
	server=${servers[$1]}

	echo "$server 	$2 objectives 	$3"
	echo iterations=${iterations[$2]}  > configurationFiles/$server/SMPSOStudy.conf
	echo size=${sizes[$2]}            >> configurationFiles/$server/SMPSOStudy.conf
	echo m=${mm[$2]}                  >> configurationFiles/$server/SMPSOStudy.conf
	echo runs=$runs                   >> configurationFiles/$server/SMPSOStudy.conf
	echo problems=${problemList[$3]}  >> configurationFiles/$server/SMPSOStudy.conf
	echo run=$algorithmsToRun         >> configurationFiles/$server/SMPSOStudy.conf

	log "cleaning configurationFiles and output folders..."
	ssh -o StrictHostKeyChecking=no -l $user $server "rm -r $server/experiment/SMPSOStudy/"
	ssh -o StrictHostKeyChecking=no -l $user $server "rm -r $server/experiment/configurationFiles/"

	log "sending new configurations..."
	rsync -azquP configurationFiles/ $user@$server:~/$server/experiment/configurationFiles/

	script="cd $server/experiment && java $java_options -cp jmetal.jar jmetal.experiments.studies.SMPSOStudy r configurationFiles/$server/SMPSOStudy.conf"
	ssh -o StrictHostKeyChecking=no -l $user $server $script

	mkdir -p experiment/$server/
	rsync -azquP $user@$server:~/$server/experiment/SMPSOStudy experiment/$server/

}

function execute () {
	init
	while [[ $i -lt ${#mm[@]} ]]; do
		for (( k = 0; (k < ${#servers[@]}) && ($i < ${#mm[@]}); k++ )); do
			if [[ ${S[$k]} -eq 0 ]] || ! ps -p ${S[$k]} > /dev/null; then
				next
				if [[ $i -lt ${#mm[@]} ]]; then
					run $k $i ${problemList[$j]} &
					S[$k]=$!
					sleep 1
				else
					break
				fi
			fi
		done
		sleep 1
	done
}

clear
compile
a=$?
echo "removing aux files..."
rm *.aux

if ! [ $a -eq 0 ]; then
	echo -e "\e[1;31m COMPILATION ERROR! \e[0m"
else 
	echo -e "\e[1;34m COMPILATION SUCCESS! \e[0m"
	execute
fi

echo "done."